INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('area_magistratura',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<div id="mainTitle">
   <h2 class="border-bottom-blue pb-2">Magistratura militare</h2>
</div>
<div class="row">
<@wp.fragment code="jacms_content_viewer_list" escapeXml=false/>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('area_magistratura_1','area_magistratura',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 

<@wp.currentPage param="code" var="currentPageCode" />

<div id="mainTitle">
   <h2 class="border-bottom-blue pb-2">Magistratura militare in costruzione... </h2>
<p>${currentPageCode}</p>
</div>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('assetto_giustizia','assetto_giustizia',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Informazioni Generali</i></b></h4>
<hr>
<p class="MsoNormal" style="TEXT-ALIGN: justify; MARGIN: 0cm 0cm 10pt" align="justify"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">Punto di partenza in una sintetica illustrazione delle disposizioni che hanno riguardo alla Giustizia Militare � l''art. 103 della Costituzione che attribuisce ai Tribunali militari, in tempo di pace, l''esercizio della giurisdizione per i reati militari commessi dagli appartenenti alle Forze Armate. <!--?xml:namespace prefix = "o" ns = "urn:schemas-microsoft-com:office:office" /--><o:p></o:p></span></span></p>
<p class="MsoNormal" style="BACKGROUND: white; TEXT-ALIGN: justify; MARGIN: 6pt 0cm 0pt"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">L''assetto della&nbsp;Giustizia Militare � oggi&nbsp;contenuto nel Decreto Legislativo 15 marzo 2010, n. 66, in supplemento ordinario n. 84 alla G.U 8 maggio 2010, n. 106<span style="mso-spacerun: yes">&nbsp; </span>- Codice dell�ordinamento Militare, con il quale si � provveduto, tra l�altro, ad una sistemazione&nbsp;dell�intera materia che qui interessa.<o:p></o:p></span></p>
<p class="MsoNormal" style="BACKGROUND: white; TEXT-ALIGN: justify; MARGIN: 6pt 0cm 0pt"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">Sono pertanto confluite in esso le disposizioni contenute nella Legge 7 maggio 1981 n.180, (Modifiche all''ordinamento giudiziario militare di pace) e L. 30 dicembre 1988, n. 561 (Istituzione del Consiglio della Magistratura Militare), oltre a quelle del DPR di attuazione del 24 marzo 1989, n. 158, nonch� le modifiche, <span style="mso-spacerun: yes">&nbsp;</span>-nella struttura e nelle competenze territoriali degli organi giudiziari requirenti e giudicanti, oltre che nella composizione dell''Organo di autogoverno (C.M.M.) - introdotte con la con la Legge del 24 dicembre 2007, n. 244, (Legge finanziaria 2008), particolarmente profonde nell�assetto dell�epoca, di cui a questo punto sembra opportuno dar conto :<o:p></o:p></span></p>
<p class="MsoListParagraph" style="BACKGROUND: white; TEXT-ALIGN: justify; MARGIN: 6pt 0cm 0pt 14.2pt; TEXT-INDENT: -14.2pt; mso-list: l0 level1 lfo1"><span style="FONT-SIZE: 10pt; FONT-FAMILY: Symbol; LINE-HEIGHT: 115%; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol"><span style="mso-list: Ignore">�<span style="FONT: 7pt &#39;Times New Roman&#39;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">sono stati ridotti da nove a tre i tribunali militari e le procure militari: il tribunale militare e la procura militare di Verona che hanno assunto la competenza territoriale relativa alle regioni Valle d''Aosta, Piemonte, Liguria, Lombardia, Trentino-Alto Adige, Veneto, Friuli-Venezia Giulia, Emilia Romagna; il tribunale militare e la procura militare di Roma che hanno assunto la competenza territoriale relativa alle regioni Toscana, Umbria, Marche, Lazio, Abruzzo e Sardegna; il tribunale militare e la procura militare di Napoli che hanno assunto la competenza territoriale relativa alle regioni Molise, Campania, Puglia, Basilicata, Calabria e Sicilia (soppressi i tribunali militari e le procure militari della Repubblica di Torino, La Spezia, Padova, Cagliari, Bari e Palermo);<o:p></o:p></span></p>
<p class="MsoListParagraph" style="BACKGROUND: white; TEXT-ALIGN: justify; MARGIN: 6pt 0cm 0pt 14.2pt; TEXT-INDENT: -14.2pt; mso-list: l0 level1 lfo1"><span style="FONT-SIZE: 10pt; FONT-FAMILY: Symbol; LINE-HEIGHT: 115%; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol"><span style="mso-list: Ignore">�<span style="FONT: 7pt &#39;Times New Roman&#39;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">sono state soppresse le sezioni distaccate di Verona e Napoli della corte militare d''appello e i relativi uffici della procura generale militare della Repubblica;<o:p></o:p></span></p>
<p class="MsoListParagraph" style="BACKGROUND: white; TEXT-ALIGN: justify; MARGIN: 6pt 0cm 0pt 14.2pt; TEXT-INDENT: -14.2pt; mso-list: l0 level1 lfo1"><span style="FONT-SIZE: 10pt; FONT-FAMILY: Symbol; LINE-HEIGHT: 115%; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol"><span style="mso-list: Ignore">�<span style="FONT: 7pt &#39;Times New Roman&#39;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">il ruolo organico dei magistrati militari � stato fissato in cinquantotto unit�.<o:p></o:p></span></p>
<p class="MsoNormal" style="TEXT-ALIGN: justify; MARGIN: 6pt 0cm 0pt"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">Alla �Giustizia Militare� il Dlgs 66/2010<span style="mso-spacerun: yes">&nbsp; </span>dedica gli articoli da 52 a 86, contenuti nel Capo VI del Libro I, a sua volta suddiviso nelle Sezioni dedicate all��Ordinamento giudiziario militare�, al �Consiglio della Magistratura Militare�, alla �Disciplina del concorso in magistratura militare� e, l�ultimo, all� �Ordinamento penitenziario militare�. <o:p></o:p></span></p>
<p class="MsoNormal" style="TEXT-ALIGN: justify; MARGIN: 6pt 0cm 0pt"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">A questi si aggiunge l�articolo 2121 �Modifiche al regio decreto 20 febbraio 1941, n. 303 � Codice penale militare di pace - con il quale vengono sostituiti gli articoli 273 cpmp <span style="mso-spacerun: yes">&nbsp;</span>(Reati commessi all�estero) e 409 cpmp (Tribunale e Ufficio militare di Sorveglianza), e inseriti gli articoli 261-ter cpmp <span style="mso-spacerun: yes">&nbsp;</span>(Ricorso per Cassazione) e 261 � quater cpmp (Giudizio davanti alla Corte militare di Appello).<o:p></o:p></span></p>
<p class="MsoNormal" style="TEXT-ALIGN: justify; MARGIN: 6pt 0cm 0pt"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">Di seguito si allega il testo del Dlgs 66/2010.</span></p>
<p class="MsoNormal" style="TEXT-ALIGN: justify; MARGIN: 6pt 0cm 0pt"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"></span>&nbsp;</p>
<p class="MsoNormal" style="TEXT-ALIGN: justify; MARGIN: 0cm 0cm 10pt" align="justify"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">Allegato n. 1 (in formato compresso): <a href="http://portalegiustiziamilitare.difesa.it/Display.aspx?Tabella=Contenuti&amp;Id=7791" target="_blank"><font color="#4682b4"><strong>Codice Ordinamento Militare - Dlgs 66/2010</strong></font></a></span></p>
              ',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('bread_crumbs','bread_crumbs',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>

<@wp.currentPage param="code" var="currentPageCode"/>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
<@wp.nav spec="current.path" var="lista">
         <#if lista.code== currentPageCode>
             <li class="breadcrumb-item active" aria-current="page">${lista.title}</li>
           <#else>
            <li class="breadcrumb-item"><a href="<@wp.url page=lista.code/>">${lista.title}</a></li>
            </#if>
</@wp.nav>
  </ol>
</nav>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cenni_generali','cenni_generali',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Cenni Generali</i></b></h4>
<hr>
<span style="height:45px; display:block"><br></span><div class="contenutoPagine">
                <div><p align="justify"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"><strong><font color="#006400">Ufficiali Giudici militari</font></strong></span></p>
<p align="justify"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"><span style="FONT-FAMILY: &#39;Arial Narrow&#39;,&#39;sans-serif&#39;; mso-bidi-font-family: Arial"><font face="Arial">I collegi giudicanti militari di cui agli articoli 54 (Tribunale militare) e 57 (Corte militare di Appello) del Dlgs. 66/2010 giudicano con l�intervento, oltre che dei magistrati togati, anche di militari con grado pari a quello dell�imputato e comunque non inferiore al grado di Ufficiale, ovvero non inferiore a tenente colonnello per la Corte militare d�Appello.<!--?xml:namespace prefix = "o" ns = "urn:schemas-microsoft-com:office:office" /--><o:p></o:p></font></span></span></p>
<p style="TEXT-ALIGN: justify; MARGIN-RIGHT: -0.05pt"><span style="FONT-FAMILY: &#39;Arial Narrow&#39;,&#39;sans-serif&#39;; mso-bidi-font-family: Arial"><font face="Arial">Riguardo la posizione da questi rivestita nei collegi anzidetti la Corte Costituzionale ha avuto modo di osservare, in una importante decisione del 1989, la n. 49, che (�) <i style="mso-bidi-font-style: normal">Se si ha riguardo al quadro generale della disciplina, non pu� certo ritenersi arbitrario che la posizione dell''ufficiale chiamato a far parte del tribunale sia stata assimilata, quanto alle ipotesi in cui lo stesso pu� incorrere nella responsabilit� civile, alla posizione dei giudici esperti presenti nei diversi tipi di organi specializzati.<o:p></o:p></i></font></span></p>
<p style="TEXT-ALIGN: justify; MARGIN-RIGHT: -0.05pt"><i style="mso-bidi-font-style: normal"><span style="FONT-FAMILY: &#39;Arial Narrow&#39;,&#39;sans-serif&#39;; mso-bidi-font-family: Arial"><font face="Arial">Significativi elementi possono trarsi dalle modalit� di scelta di detto componente. Esso viene infatti estratto a sorte soltanto tra gli ufficiali che prestano servizio nella circoscrizione del tribunale militare, senza distinzione circa l''arma di appartenenza. E'' da escludere, quindi, che possa parlarsi, come si fa nell''ordinanza di rimessione, di analogia con la figura del giudice popolare e comunque della caratterizzazione dell''ufficiale membro del collegio quale esponente della istituzione cui appartiene l''imputato. Al contrario, la scelta tra i soli ufficiali e indice dell''intenzione di assicurare al collegio l''apporto di persona dotata di un buon livello culturale e di quelle cognizioni pi� ampie e pi� complete che vengono dall''inserimento in compiti di maggiore responsabilit�. A sua volta, l''indifferenza circa l''arma di appartenenza del prescelto rende evidente che il legislatore ha inteso valorizzare la conoscenza della vita militare nel suo ordinamento e nella sua organizzazione in termini generali e non gi� il sentimento ed il giudizio di coloro che in concreto condividono l''esperienza della pi� circoscritta unita territoriale in cui l''imputato si e trovato a prestare il proprio servizio.<o:p></o:p></font></span></i></p>
<p style="TEXT-ALIGN: justify; MARGIN-RIGHT: -0.05pt"><i style="mso-bidi-font-style: normal"><span style="FONT-FAMILY: &#39;Arial Narrow&#39;,&#39;sans-serif&#39;; mso-bidi-font-family: Arial"><font face="Arial">Le particolarit� della normativa inducono quindi a ritenere che l''ufficiale membro del collegio sia chiamato a dare un qualificato contributo inerente alla peculiarit� della vita e dell''organizzazione militare: contributo consistente nell''aiutare il collegio a fondare le proprie valutazioni sulla piena conoscenza e la piena comprensione dei molteplici aspetti del concreto atteggiarsi di quel settore; delle condizioni che lo caratterizzano e dei problemi che vi si pongono. Aspetti tutti che non possono non riflettersi sulla ricostruzione e valutazione degli elementi oggettivi e soggettivi dei fatti-reato sottoposti al giudizio del tribunale, anche alla luce di quei valori tipici dell''ordinamento militare che gi� la Corte ha ritenuto tali da concorrere a giustificare l''esistenza della speciale giurisdizione (sentenza 22 luglio 1976, n. 192).(.....)�</font></span></i></p>
<p style="TEXT-ALIGN: justify; MARGIN-RIGHT: -0.05pt"><i style="mso-bidi-font-style: normal"><span style="FONT-FAMILY: &#39;Arial Narrow&#39;,&#39;sans-serif&#39;; mso-bidi-font-family: Arial"><font size="3"></font></span></i>&nbsp;</p>
 ',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cenni_storici','cenni_storici',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Cenni Storici</i></b></h4>
<hr>
<p style="LINE-HEIGHT: normal; MARGIN: 6pt 0cm 0pt; BACKGROUND: white; mso-outline-level: 4" class="MsoNormal"><b style="mso-bidi-font-weight: normal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #163d5f; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT"><font color="#006400">Origini e vicende della Giustizia Militare in Italia<!--?xml:namespace prefix = o ns = "urn:schemas-microsoft-com:office:office" /--><o:p></o:p></font></span></b></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">Le origini dell''istituto della Giustizia Militare risalgono alla legislazione in materia dello Stato sardo, promulgata da Vittorio Emanuele II il 1� ottobre 1850 e rimasta in vigore nei primi anni dello Stato unitario.<o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">Nel periodo precedente, la "magistratura criminale militare" era amministrata, nello stesso Stato, da consigli di guerra di reggimento e di divisione ordinaria o subitanei, da consigli di guerra misti, dall''uditore generale di guerra. La competenza dei vari consigli era fissata in ragione della gravit� del reato, della qualit� degli imputati nonch� dall''esigenza di esemplarit�. Commissioni di inchiesta provvedevano alla istruzione dei procedimenti ed al deferimento degli imputati ai consigli di guerra ordinari. Ufficiali in servizio componevano le commissioni ed i consigli e svolgevano presso di essi le funzioni di pubblico ministero. Estensore della sentenza era, invece, un uditore di guerra reggimentale o divisionario, assimilato al grado di ufficiale. <o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">Preposto all''amministrazione della Giustizia Militare era l''uditore generale di guerra, organo creato nel 1582 da Emanuele Filiberto per l''esercizio della giurisdizione militare nello stato sabaudo. La giustizia militare negli stati italiani preunitari era amministrata da collegi militari consimili ai consigli di guerra di cui si � detto.<o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">Il codice penale militare del 1� ottobre 1859 prevedeva la giustizia militare amministrata da commissioni di inchiesta, da tribunali militari territoriali, da tribunali presso truppe e da un tribunale supremo di guerra; quest''ultimo, destinato a conoscere dei ricorsi in nullit� contro le sentenze emanate dai tribunali militari, ebbe, nella legislazione di un decennio successiva (Codice penale militare per l''Esercito e Codice penale militare marittimo, entrambi del 28 novembre 1869), la denominazione di tribunale supremo di guerra e di marina e, quindi, dal 1923, la denominazione, durata fino al 1981, di tribunale supremo militare.<o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">I tribunali militari erano composti da sei ufficiali in servizio, compreso il presidente, colonnello o tenente colonnello, e assistiti nella deliberazione da un segretario, estensore della sentenza. L''Ufficio del pubblico ministero presso il tribunale militare era affidato ad un avvocato fiscale militare, facente capo all''avvocato generale militare, che svolgeva le funzioni di pubblico ministero presso il tribunale supremo militare di guerra.<o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">Dal 1859 fino alla prima guerra mondiale si erano avuti, oltre al tribunale supremo, dodici tribunali militari, ciascuno dei quali presso un comando di corpo d''armata, e quattro tribunali militari marittimi, presso altrettanti dipartimenti. Esigenze proprie del periodo bellico determinarono la costituzione: di tribunali di guerra in zona territoriale; di corpo d''armata mobilitato; d''armata, d''indipendenza o di tappa; marittimi; di piazzaforte e infine all''estero, per un totale di oltre cento unit�. Allo svolgimento delle attivit� di tali organi giurisdizionali concorrevano, con il personale della Giustizia Militare, militarizzato nel 1916, magistrati ordinari con assimilazione di grado militare e ufficiali laureati in legge.<o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">Nel 1918, il personale della Giustizia Militare, diviso nelle categorie del servizio attivo permanente e di complemento, entr� a far parte dell''esercito; la figura del segretario estensore fu sostituita, nel collegio giudicante, da un giudice relatore, magistrato militare, con voto deliberativo.<o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">Nel 1923, il numero dei tribunali militari torn� ad essere quello di 12 anteriore alla guerra; il personale della Giustizia Militare cess� di far parte dell''Esercito; venne istituito il ruolo dei cancellieri militari. Nel 1931, la presidenza dei tribunali militari territoriali venne affidata ad un ufficiale generale di brigata. Nello stesso anno nacque la figura del consigliere relatore del tribunale supremo militare per sostituire, in quel supremo collegio, il consigliere di Stato.<o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">Per soddisfare le esigenze della mobilitazione, e rendere immediatamente funzionanti i tribunali militari nell''ipotesi di guerra, fu istituito, con R.D. 28.11.1935, n.2397, convertito nella L. 6.4.1936, n.818, il Corpo degli ufficiali in congedo della Giustizia militare, distinto in tre ruoli: ordinario, di riserva, ausiliario. I primi due sono costituiti sin dal tempo di pace, il terzo soltanto in caso di mobilitazione. Ciascun ruolo � diviso in due categorie: magistrati e cancellieri, la nomina dei quali � stabilita in dettaglio dalla legge istitutiva. Il predetto Corpo fa parte in dettaglio dalla legge pur lasciando intatto, in tempo di pace, al magistrato militare lo stato giuridico civile.<o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">I magistrati e i cancellieri militari che, sino al dicembre 1974 indossavano, in udienza, la grande uniforme del Corpo in congedo degli ufficiali della Giustizia militare, dal gennaio 1975 indossano la toga.<o:p></o:p></span></p>
<p style="LINE-HEIGHT: normal; MARGIN: 6pt 0cm 0pt; BACKGROUND: white; mso-outline-level: 4" class="MsoNormal"><b style="mso-bidi-font-weight: normal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #163d5f; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT"><o:p></o:p></span></b></p>
<p style="LINE-HEIGHT: normal; MARGIN: 6pt 0cm 0pt; BACKGROUND: white; mso-outline-level: 4" class="MsoNormal"><b style="mso-bidi-font-weight: normal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #163d5f; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT"><font color="#006400">La riforma della Giustizia Militare e la organizzazione attuale<o:p></o:p></font></span></b></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">Una profonda riforma dell''ordinamento giudiziario militare di pace si � avuta con la Legge 7.5.1981 n.180, che ha in particolare istituito la Corte militare di appello, ha soppresso, per il tempo di pace, il Tribunale supremo militare, ed ha previsto la competenza per il giudizio di legittimit� della Corte di cassazione, presso cui � istituito un Ufficio del Pubblico ministero militare. L''art. 1 della L. 180/1981, inoltre, ha esteso ai magistrati militari le medesime garanzie e le stesse norme di avanzamento previste per la magistratura ordinaria, e la L. 30.12.1988, n.561, ha istituito, come organo di autogoverno, il Consiglio della magistratura militare, composto originariamente dal Primo Presidente della Corte di cassazione, con funzioni di Presidente, da due membri laici (nominati, di concerto, dai Presidenti dei due rami del parlamento), dal Procuratore generale militare presso la Corte di cassazione (membro di diritto) e da cinque magistrati militari (uno dei quali con funzioni di cassazione) eletti da tutti i magistrati militari.<o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">L�art. 2, commi 603-611 della l. 24 dicembre 2007, n. 244 ha soppresso le sedi di Tribunale militare di Torino, La Spezia, Padova, Cagliari, Bari e Palermo, nonch� le Sezioni distaccate di Corte militare di appello di Verona e Napoli. Sono pertanto rimasti, rispetto alle previsioni del d.P.R. 14 febbraio 1964, i soli Tribunali militari di Verona, Roma e Napoli, mentre vi sono una unica Corte militare di appello ed un unico Tribunale militare di sorveglianza, con sede in Roma.<o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">La l. 3 agosto 2009, n. 102 ha modificato la composizione del Consiglio della magistratura militare, che � ora costituito da cinque membri: il Primo Presidente della Corte di cassazione, con funzioni di Presidente, un membro laico scelto d�intesa dai Presidenti del Senato e della Camera dei deputati, con funzioni di Vicepresidente, il Procuratore generale militare presso la Corte di cassazione e due componenti eletti dai magistrati militari che sono posti fuori ruolo per la durata del mandato.<o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; TEXT-INDENT: 35.4pt; MARGIN: 6pt 0cm 0pt; BACKGROUND: white" class="MsoNormal"><span style="FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; COLOR: #333333; FONT-SIZE: 10pt; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-fareast-language: IT">Attualmente le norme in materia di ordinamento giudiziario militare, Consiglio della magistratura militare, disciplina del concorso in magistratura militare e ordinamento penitenziario militare sono inserite nel Codice dell�ordinamento militare, approvato con d. lgs. 15 marzo 2010, n. 66, Libro I, Titolo III, capo VI, artt. 52 � 86. <o:p></o:p></span></p>
<p style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; MARGIN: 6pt 0cm 0pt" class="MsoNormal"><span style="FONT-SIZE: 10pt"><o:p><font face="Calibri"></font></o:p></span></p>
 ',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('common_footer','common_footer',NULL,'<#assign wp=JspTaglibs[ "/aps-core" ]>
    <div class="row mt-4 mb-4">
        <div class="col-2">
            <img src="<@wp.resourceURL/>static/img/logo_repubblica.png" alt="Logo Stato Maggiore della Difesa"
                class="logo-footer" />
        </div>
        <div class="col-10 text-white">
            <p class="h2 pt-md-3" id="title-footer">Ministero della Difesa
                <br />Portale Archimede</p>
        </div>
    </div>
    <div class="row mt-2">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card bg-transparent border-0">
                        <div class="card-header bg-transparent border-bottom-white border-radius-0">
                            <p class="h4 text-white">Ministero della Difesa</p>
                        </div>
                        <ul class="card-body list-unstyled">
                            <li><a href="<@wp.url page=''ministero'' />"> Gabinetto del Ministero</a></li>
                            <li><a href="<@wp.url page=''stato_maggiore'' />"> Stato Maggiore Difesa</a></li>
                            <li><a href="https://intranet.sgd.difesa.it/Pagine/Home.aspx" target="_blank"> SGD-DNA</a>
                            </li>
                            <li><a href="http://www.sme.esercito.difesa.it/" target="_blank"> Esercito</a></li>
                            <li><a href="https://portale.marina.difesa.it/" target="_blank"> Marina</a></li>
                            <li><a href="http://www.aeronautica.difesa.it/" target="_blank"> Aeronautica</a></li>
                            <li><a href=" https://servizi.carabinieri.difesa.it/" target="_blank"> Carabinieri</a></li>
                            <li><a href="<@wp.url page=''magistratura_militare'' />"> Magistratura Militare</a></li>
                            <li><a href="<@wp.url page=''sala_stampa'' />"> Rassegna Stampa</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-transparent border-0">
                        <div class="card-header bg-transparent border-bottom-white border-radius-0">
                            <p class="h4 text-white">Rassegna Stampa</p>
                        </div>
                        <ul class="card-body list-unstyled">
                            <li><a href="<@wp.url page=''sala_stampa'' />">Rassegna stampa</a></li>
                            <li><a href="<@wp.url page=''in_progress'' />">Infografiche</a></li>
                            <li><a href="<@wp.url page=''campagne_informative'' />">Campagne Informative</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-transparent border-0">
                        <div class="card-header bg-transparent border-bottom-white border-radius-0">
                            <p class="h4 text-white">Servizi</p>
                        </div>
                        <ul class="card-body list-unstyled">
                            <li><a href="<@wp.url page=''direttive'' />" target="_blank">Direttive</a></li>
                            <li><a href="https://archimedeold.difesa.it/Intranet/default.htm">Vecchio Portale Archimede</a></li>
                            <li><a href="<@wp.url page=''servizi'' />">Macroaree Servizi</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-2">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card bg-transparent border-0">
                        <div class="card-header bg-transparent border-bottom-white border-radius-0">
                            <p class="h4 text-white">Contatti</p>
                        </div>
                        <div class="card-body">
                            <address class="text-white">
                                Centralino Tel. (+39) 06-4882126<br>
                            </address>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-transparent border-0">
                        <div class="card-header bg-transparent border-bottom-white border-radius-0">
                            <p class="h4 text-white">Indirizzi di Posta Elettronica</p>
                        </div>
                    </div>
                    <ul class="card-body list-unstyled">
                        <li class="text-white">Email Istituzionale:</li>
                        <a href="mailto:stamadifesa@smd.difesa.it">stamadifesa@smd.difesa.it</a>
                        <li class="text-white">Email di Posta Certificata:</li>
                        <a href="mailto:stamadifesa@postacert.difesa.it">stamadifesa@postacert.difesa.it</a>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="card bg-transparent border-0">
                        <div class="card-header bg-transparent border-bottom-white border-radius-0">
                            <p class="h4 text-white">Dove Siamo</p>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <a href="<@wp.url page=''mappa''/>">
                            <div class="osm-location container"
                                style="width: 300px;margin-top: 5px;text-align: center;">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="container-fluid bottom-div">
                                            <p>Via XX Settembre, 123/a
                                                <br> Roma, 00187</p>
                                        </div>
                                    </div>
                                </div>
                                <i class="fa fa-map-marker-alt fa-3x"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-2">
        <div class="container border-top border-top-white pt-2">
            <div class="row">
                <div class="col-md-7">
                    <p class="text-white h6">&copy; 2019 - Ministero della Difesa</p>
                </div>
                <div class="col-md-5">
                    <ul class="list-inline float-md-right">
                        <li class="list-inline-item"><a href="<@wp.url page=''in_progress'' />" title=""><span>Note
                                    Legali</span></a>
                        </li>
                        <li class="list-inline-item"><a href="<@wp.url page=''in_progress'' />"
                                title=""><span>Privacy</span></a></liclass>
                        <li class="list-inline-item"><a href="<@wp.url page=''in_progress'' />" title="Mappa del
                  Sito"><span>Mappa del Sito</span></a>
                        </li>
                        <li class="list-inline-item"><a href="<@wp.url page=''in_progress'' />"
                                title=""><span>Redazione</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalSearch" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="<@wp.url page=''advanced_search_result'' />">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ricerca Avanzata</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="form-group">
                                <!-- Date input -->
                                <label class="control-label" for="date" style="font-size: 1.14rem;">Cerca per Data <i
                                        class="fa fa-calendar-alt" style="color: #0069d9"></i></label>
                                <input class="form-control" id="date" name="date" type="text" />
                                <script type="text/javascript">
                                    $("#date").datepicker({
                                        format: ''dd/mm/yyyy'',
                                        todayHighlight: true,
                                        autoclose: true,
                                    });
                                </script>
                            </div>
                            <div class="form-group">
                                <label for="inlineFormCustomSelect" style="font-size: 1.14rem;">Cerca per Formato File
                                    <i class="fa fa-file" style="color: #0069d9"></i></label>
                                <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                    <option selected>Seleziona...</option>
                                    <option value="1">PDF</option>
                                    <option value="2">Word</option>
                                    <option value="3">Excell</option>
                                    <option value="4">Video</option>
                                    <option value="5">Immagine</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inlineFormCustomSelect2" style="font-size: 1.14rem;">Cerca per Tags <i
                                        class="fa fa-hashtag" style="color: #0069d9"></i></label>
                                <select class="custom-select" id="inlineFormCustomSelect2">
                                    <option selected>Seleziona...</option>
                                    <option value="1">Aeronautica Militare</option>
                                    <option value="2">Capo di Stato Maggiore</option>
                                    <option value="3">Esercito</option>
                                    <option value="4">Marina Militare</option>
                                    <option value="5">Ministero</option>
                                    <option value="6">Ministro</option>
                                    <option value="7">Sottocapo di Stato Maggiore</option>
                                    <option value="8">VI Reparto</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cerca</button>
                    </div>
                </form>
            </div>
        </div>
    </div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('common_header','common_header',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 


<div class="container-fluid header-title pt-1" id="main-header">
    <div class="row">
        <div class="container">
            <div class="row">
			
                <div class="col-lg-5 col-md-5 col-8 mt-lg-2"> 
				<a href="<@wp.url page=''homepage''/>"> 
				<img src="<@wp.resourceURL/>static/img/logo_repubblica_top.png" class="logo float-left mr-3"/>
                        <h2 class="mt-2 site-title"> Magistartura Miliatre <br /> Portale Giustizia </h2>
                    </a> 
				</div>
				
				<div class="col-2 col-md-6 d-md-none icon-button-mobile align-self-center"> 
				   <i class="fa fa-search fa-2x" id="search-button-mobile"></i> 
				</div>
                <div class="col-2 col-md-6 d-md-none icon-button-mobile align-self-center"> 
				   <i class="fa fa-bars fa-2x hamburger-button"></i> 
				</div>
	
                <div class="col-lg-5 col-md-5 mt-md-3" id="search-rubrica">
				  <div class="d-none d-md-block ">
                    <form action="<@wp.url />" id="searh-in-form" class="input-group"> 
					<input class="form-control" name="search" placeholder="Cerca nel sito" id="search-in"> 
					<input type="hidden" name="search-type" id="search-type-id">
                        <div class="input-group-append"> 
						<button class="btn btn-pa-default dropdown-toggle d-none d-sm-inline" data-toggle="dropdown" id="button-search-in">Tipo ricerca</button>
                            <div class="dropdown-menu"> 
							        <a class="dropdown-item" value="site" href="#" id="search-in-sito">Ricerca nel sito</a> 
									<a class="dropdown-item" value="rubrica" href="#" id="search-in-rubrica">Ricerca nella rubrica</a> 
									<a class="dropdown-item d-block d-sm-none" href="#" id="search-in-rubrica">Ricerca avanzata</a> 
							</div>
                        </div>
                        <div class="input-group-append"> 
						<button class="btn btn-pa-default fa fa-search"></button>
                        </div>
                    </form>
                    <a href="#" class="d-none d-sm-block" data-toggle="modal" data-target="#exampleModalSearch"> Apri
                        pannello ricerca avanzata </a>
						
				  </div>
						
				  <div style="visibility:hidden" id="accediForm">
				    <form method="POST">
				
				      <div class="input-group mt-2 mb-3 col-xs mb-xs ">
					  <input type="text" class="form-control w-25" placeholder="Dominio\username" name="username">
				      <input type="password" class="form-control" placeholder="Password" name="password">

				        <div class="input-group-append ">
				           <input type="submit" value="LogIn" class="btn btn-primary"/>
                        </div>
					  </div>
				<!--
				         <a class="btn btn-primary text-light w-100" href="<@wp.i18n key=''mutual_ssl_auth_url'' />?returnUrl=<@wp.url page=''autenticazione_smart_card'' />"> Accedi con CMD <i class="fa fa-id-card"></i>
                         </a>
                               -->
                    </form> 
				  </div>
						
						
                </div>
				
             <style>
             .myScolled {
                         margin-top: 0.5rem !important;
                         position: sticky;
                         }
             </style>
				
		    <script>
				window.addEventListener(''scroll'', function() {
			   if(document.getElementById("welcome-account")){
		         if (window.pageYOffset > 100) {
                     document.getElementById("welcome-account").classList.add("myScolled");
                  } else {
                     document.getElementById("welcome-account").classList.remove("myScolled");
                  }
				  };
				});
				
			</script>
				
				
				
				
				
		<#if (Session.currentUser !="guest" )>
          <div class="col-lg-2 col-md-2 col-12 mb-xs">
           <!-- ${Session.currentUser} -->
           <@wp.currentUserProfileAttribute attributeRoleName="userprofile:fullname" var="fullNameVar" />
            <div class="text-right text-white mt-lg-3 mb-sm-2 text-center" id="welcome-account">
			
			<!-- Default dropright button -->
               <div class="btn-group dropdown" >
                 <button type="button" class="btn btn-info dropdown-toggle ml-2" data-toggle="dropdown">
                    Benvenuto
                    <#if (fullNameVar?? && fullNameVar?has_content)>
                        ${fullNameVar}
                        <#else>
                            ${Session.currentUser}
                    </#if>
                 </button>
				 
                  <div class="dropdown-menu">
                    <!-- Area amministrazione -->
                    <@wp.ifauthorized permission="enterBackend">
                        <a class="dropdown-item" href="<@wp.info key=''systemParam'' paramName=''applicationBaseURL'' />do/main.action?request_locale=<@wp.info key=''currentLang'' />">
                            <span class="icon-wrench"></span>Area di Amministrazione
                        </a>
                    </@wp.ifauthorized> <!-- Disconnetti -->
                    <a class="dropdown-item"
                        href="<@wp.info key=''systemParam'' paramName=''applicationBaseURL'' />do/logout.action">Disconnetti</a>
                </div>
               </div>
						 
  
			
            <@wp.pageWithWidget var="editProfilePageVar" widgetTypeCode="userprofile_editCurrentUser" />
            <#if (editProfilePageVar??)>
                <p class="help-block text-right">
                    <a href="<@wp.url page=''${editProfilePageVar.code}''/>">Configurazione profilo</a>
                </p>
            </#if>
              </div>
            </div>

			
	         <#else>
             <#if ((wrongAccountCredential?? && wrongAccountCredential==true) || (accountExpired?? && accountExpired==true))>
            <div class="alert alert-danger text-center alert-dismissible fade show" role="alert">
                <strong>Accesso non riuscito: </strong> Credenziali non valide 
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"> 
				<span aria-hidden="true">&times;</span> 
				</button>
             </div>
             </#if>
			 
			 
			 
		<script>
			 function hiddenAccediCMD(){
			 document.getElementById("linkAccediCMD").style.visibility = "hidden";
			 document.getElementById("accediForm").style.visibility="visible";
			 }
		</script>
			 
		 <style>
            .myScolledAccedi {
             margin-top: 1rem !important;
             position: sticky;
             }
         </style>
			 
		 <script>
		   window.addEventListener(''scroll'', function() {
		    if(document.getElementById("linkAccediCMD").style.visibility == "visible"){
		       if (window.pageYOffset > 100) {
                   document.getElementById("accedi").classList.add("myScolledAccedi");
                 } else {
                 document.getElementById("accedi").classList.remove("myScolledAccedi");
                 }
		     } 
		   });	
		</script>
				
				
				<div class="col-lg-2 col-md-2 mt-md-4 col-12 text-center" id="accedi">
				
				  <a href=# onclick="hiddenAccediCMD()" style="visibility:visible;" id="linkAccediCMD" class="" >Accedi con CMD <i class="fa fa-id-card"></i> </a>	

				
			    </div>
               
	           </#if>
			
            </div>

        </div>
    </div>
	
	
	
	
    <div class="row h-8px">
        <div class="col-4 bg-success"> 
		</div>
        <div class="col-4 bg-white"> 
		</div>
        <div class="col-4 bg-danger"> 
		</div>
    </div>
	
	
            <div class="hamburger">
                <nav>
                    <div class="menu-header"> <i class="fa fa-times close-menu"></i> </div>
                    <ul>
                        <li><a href="<@wp.url page=''homepage''/>">Home</a></li>
                        <li><a href="<@wp.url page=''ministero''/>">Gabinetto Ministro</a></li>
                        <li><a href="<@wp.url page=''stato_maggiore''/>">Stato Maggiore Difesa</a></li>
                        <li><a href="http://www.sme.esercito.difesa.it/">Esercito</a></li>
                        <li><a href="https://portale.marina.difesa.it/">Marina</a></li>
                        <li><a href="http://www.aeronautica.difesa.it/">Aeronautica</a></li>
                        <li><a href=" https://servizi.carabinieri.difesa.it/">Carabinieri</a></li>
                        <li><a href="https://intranet.sgd.difesa.it/Pagine/Home.aspx">SGD-DNA</a></li>
                        <li><a href="<@wp.url page=''sala_stampa''/>">Rassegna Stampa</a></li>
                        <li><a href="<@wp.url page=''rubrica''/>">Rubrica</a></li>
                    </ul>
                </nav>
            </div>
	
	

	
	
    <nav class="navbar navbar-expand-md navbar-light" id="horizontal-menu">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mx-auto" id="horizontalMenu">
                <li class="nav-item nav-link-margin"> <a class="nav-linkr text-white"
                        href="<@wp.url page=''ministero''/>">Gabinetto del Ministro</a> </li>
                <li class="nav-item nav-link-margin"> <a class="nav-linkr text-white"
                        href="<@wp.url page=''stato_maggiore''/>">Stato Maggiore Difesa</a> </li>
                <li class="nav-item nav-link-margin"> <a class="nav-linkr text-white"
                        href="https://intranet.sgd.difesa.it/Pagine/Home.aspx" target="_blank">SGD-DNA</a> </li>
                <li class="nav-item nav-link-margin"> <a class="nav-linkr text-white"
                        href="http://www.sme.esercito.difesa.it/" target="_blank">Esercito</a> </li>
                <li class="nav-item nav-link-margin"> <a class="nav-linkr text-white"
                        href="https://portale.marina.difesa.it/" target="_blank">Marina</a> </li>
                <li class="nav-item nav-link-margin"> <a class="nav-linkr text-white"
                        href="http://www.aeronautica.difesa.it/" target="_blank">Aeronautica</a> </li>
                <li class="nav-item nav-link-margin"> <a class="nav-linkr text-white"
                        href=" https://servizi.carabinieri.difesa.it/" target="_blank">Carabinieri</a> </li>
                <li class="nav-item nav-link-margin"> <a class="nav-linkr text-white"
                        href="<@wp.url page=''magistratura_militare''/>">Magistratura Militare</a> </li>
                <li class="nav-item nav-link-margin"> <a class="nav-linkr text-white"
                        href="<@wp.url page=''sala_stampa''/>">Rassegna Stampa</a> </li>
                <li class="nav-item nav-link-margin"> <a class="nav-linkr text-white"
                        href="<@wp.url page=''rubrica''/>">Rubrica</a> </li>
            </ul>
        </div>
    </nav>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('consiglio_magistratura','consiglio_magistratura',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
 <div class="pt-4 pb-4" style="background-color: #eee;">
        <div class="row">
            <div class="col-2" align="right" >
                <img style="box-shadow: 5px 5px 5px 0px #000000; width: 5rem; height: 5rem;" src="/SIGMIL/resources/cms/images/Palazzo_Cesi_Facciata_1.jpg"">
            </div>
            <div class="col-10">
                    <h4 style="color: #228B22;"><b>Normativa</b></h4>
                   <hr style=" border: 1.2px solid green;margin-right: 30px;">
<i>Organo di autogoverno della Magistratura Militare</i>
            </div>
        </div>
    </div>
<hr>
<div class="container" align="center">
<img src="/SIGMIL/resources/cms/images/Palazzo_Cesi_Facciata_1.jpg">
<p> <i>Palazzo CESI in Roma, Sede del Consiglio della Magistratura Militare</i></p>
</div>

<div id="mainAccordion">
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''dati_generali''/>">Dati generali</a>
               </h5>
         </div>
      </div>
   </div>
 <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''composizione_del_consiglio''/>">Composizione del Consiglio</a><br>
                  Le Consiliature a partire dal 12 giugno 1989
               </h5>
         </div>
      </div>
   </div>
 <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''attribuzioni_e_normativa''/>">Attribuzioni e Normativa</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''attribuzioni_mindif''/>">Attribuzioni del Ministro della Difesa</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''comitato_pari_opportunita''/>">Comitato per le Pari Opportunit�</a>
<br>Attivit� e documentazione informativa del Comitato per le Pari Opportunit�
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''attribuzioni_mindif''/>">Attribuzioni del Ministro della Difesa</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''comitato_pari_opportunita''/>">Comitato per le Pari Opportunit�</a>
<br>Attivit� e documentazione informativa del Comitato per le Pari Opportunit�
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''attribuzioni_mindif''/>">Attribuzioni del Ministro della Difesa</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''attivit_aistituzionale_del_cmm''/>">Attivit� istituzionale del CMM</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''regolamenti''/>">Regolamenti</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''circolari''/>">Circolari</a>
<br>Ordinate per anno e data di emissione
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''quaderni_del_cmm''/>">Quaderni del CMM</a>
<br>Raccolta delle deliberazioni del CMM
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''incontri_di_studio''/>">Incontri di studio</a>
<br>Organizzati dal Consiglio della Magistratura Militare
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''ruolo_dei_magistrati_militari''/>">Ruolo dei Magistrati Militari</a>
<br>Ruolo organico dei Magistrati Militari
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''incarichi_extragiudiziari''/>">Incarichi Extragiudiziari</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''corsi_scuola_superiore_mag''/>">Corsi della Scuola Superiore della Magistratura</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''delibere_1''/>">Delibere 1</a>
<br>NB: funzioni riservate esclusivamente alla Segreteria del CMM
               </h5>
         </div>
      </div>
   </div>
</div>

',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('default_pagerBlock',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<#if (group.size > group.max)>
	<div class="pagination pagination-centered">
		<ul>
		<#if (1 != group.currItem)>
			<#if (group.advanced)>
				<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >1</@wp.parameter></@wp.url>" title="<@wp.i18n key="PAGER_FIRST" />"><i class="icon-fast-backward"></i></a></li>
				<#if (1 != group.beginItemAnchor)>
					<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${group.currItem - group.offset}</@wp.parameter></@wp.url>" title="<@wp.i18n key="PAGER_STEP_BACKWARD" />&#32;${group.offset}"><i class="icon-step-backward"></i></a></li>
				</#if>
			</#if>
			<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${group.prevItem}</@wp.parameter></@wp.url>"><@wp.i18n key="PAGER_PREV" /></a></li>
		</#if>
		<#list group.items as item>
		<#if (item_index >= (group.beginItemAnchor-1)) && (item_index <= (group.endItemAnchor-1))>
			<#if (item == group.currItem)>
			<li class="active"><a href="#">${item}</a></li>
			<#else>
			<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${item}</@wp.parameter></@wp.url>">${item}</a></li>
			</#if>
		</#if>
		</#list>
		<#if (group.maxItem != group.currItem)>
			<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${group.nextItem}</@wp.parameter></@wp.url>"><@wp.i18n key="PAGER_NEXT" /></a></li>
			<#if (group.advanced)>
				<#if (group.maxItem != group.endItemAnchor)>
					<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${group.currItem + group.offset}</@wp.parameter></@wp.url>" title="<@wp.i18n key="PAGER_STEP_FORWARD" />&#32;${group.offset}"><i class="icon-step-forward"></i></a></li>
				</#if>
				<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${group.maxItem}</@wp.parameter></@wp.url>" title="<@wp.i18n key="PAGER_LAST" />"><i class="icon-fast-forward"></i></a></li>
			</#if>
		</#if>
		</ul>
	</div>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('default_pagerFormBlock_is',NULL,NULL,NULL,'<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<#assign s=JspTaglibs["/struts-tags"]>
<@s.if test="#group.size > #group.max">
<ul class="pagination">
	<@s.if test="null != #group.pagerId">
		<@s.set var="pagerIdMarker" value="#group.pagerId" />
	</@s.if>
	<@s.else>
		<@s.set var="pagerIdMarker">pagerItem</@s.set>
	</@s.else>
	<@s.if test="#group.advanced">
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_1''}" type="button" disabled="%{1 == #group.currItem}" title="%{getText(''label.goToFirst'')}">
			<span class="icon fa fa-step-backward"></span>
		</@wpsf.submit>
	</li>
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_'' + (#group.currItem - #group.offset) }" type="button" disabled="%{1 == #group.beginItemAnchor}" title="%{getText(''label.jump'') + '' '' + #group.offset + '' '' + getText(''label.backward'')}">
			<span class="icon fa fa-fast-backward"></span>
		</@wpsf.submit>
	</li>
	</@s.if>
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_'' + #group.prevItem}" type="button" title="%{getText(''label.prev.full'')}" disabled="%{1 == #group.currItem}">
			<span class="icon fa fa-long-arrow-left"></span>
		</@wpsf.submit>
	</li>
	<@s.subset source="#group.items" count="#group.endItemAnchor-#group.beginItemAnchor+1" start="#group.beginItemAnchor-1">
		<@s.iterator id="item">
			<li>
				<@wpsf.submit name="%{#pagerIdMarker + ''_'' + #item}" type="button" disabled="%{#item == #group.currItem}">
					<@s.property value="%{#item}" />
				</@wpsf.submit>
			</li>
		</@s.iterator>
	</@s.subset>
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_'' + #group.nextItem}" type="button" title="%{getText(''label.next.full'')}" disabled="%{#group.maxItem == #group.currItem}">
			<span class="icon fa fa-long-arrow-right"></span>
		</@wpsf.submit>
	</li>
	<@s.if test="#group.advanced">
	<@s.set var="jumpForwardStep" value="#group.currItem + #group.offset"></@s.set>
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_'' + (#jumpForwardStep)}" type="button" disabled="%{#group.maxItem == #group.endItemAnchor}" title="%{getText(''label.jump'') + '' '' + #group.offset + '' '' + getText(''label.forward'')}">
			<span class="icon fa fa-fast-forward"></span>
		</@wpsf.submit>
	</li>
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_'' + #group.size}" type="button" disabled="%{#group.maxItem == #group.currItem}" title="%{getText(''label.goToLast'')}">
			<span class="icon fa fa-step-forward"></span>
		</@wpsf.submit>
	</li>
	</@s.if>
	<@s.set var="pagerIdMarker" value="null" />
</ul>
</@s.if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('default_pagerInfo_is',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<p><@s.text name="note.searchIntro" />&#32;<@s.property value="#group.size" />&#32;<@s.text name="note.searchOutro" />.<br />
<@s.text name="label.page" />: [<@s.property value="#group.currItem" />/<@s.property value="#group.maxItem" />].</p>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('documentazione_sigmil','documentazione_sigmil',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Documenti programma SIGMIL</i></b></h4>
<hr>
<div id="mainAccordion">
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="#file''/>">Presentazione elementi di base SIGMIL </a><br><i>(file pps)</i>
               </h5>
         </div>
      </div>
   </div>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entando-widget-language_choose','entando-widget-language_choose',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.headInfo type="JS" info="entando-misc-jquery/jquery-3.4.1.min.js" />
<@wp.headInfo type="JS" info="entando-misc-bootstrap/bootstrap.min.js" />
<@wp.info key="langs" var="langsVar" />
<@wp.info key="currentLang" var="currentLangVar" />
<ul class="nav">
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="<@wp.i18n key="ESLC_LANGUAGE" />"><span class="icon-flag"></span><span class="caret"></span></a>
      <ul class="dropdown-menu">
		<@wp.freemarkerTemplateParameter var="langsListVar" valueName="langsVar" removeOnEndTag=true >
		<#list langsListVar as curLangVar>
		<li <#if (curLangVar.code == currentLangVar)>class="active" </#if>>
			<a href="<@wp.url lang="${curLangVar.code}" paramRepeat=true />">
				<@wp.i18n key="ESLC_LANG_${curLangVar.code}" />
			</a>
		</li>
		</#list>
		</@wp.freemarkerTemplateParameter>
      </ul>
  </li>
</ul>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entando-widget-language_choose_inspinia','entando-widget-language_choose_inspinia',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.info key="langs" var="langsVar" />
<@wp.info key="currentLang" var="currentLangVar" />
<a data-toggle="dropdown" class="dropdown-toggle" href="#"  title="<@wp.i18n key="ESLC_LANGUAGE" />">     
   <#if (accountExpired?? && accountExpired == true) || (wrongAccountCredential?? && wrongAccountCredential == true)>open</#if>
   <#if (Session.currentUser != "guest")>
   <span class="block m-t-xs"> 
        <strong class="font-bold">
            ${Session.currentUser}
        </strong>
    </span>
    <#else>
    <span class="block m-t-xs"> 
        <strong class="font-bold">
            <@wp.i18n key="ESLF_SIGNIN" />
        </strong>
    </span>
    </#if>
    <span class="text-muted text-xs block">
        <@wp.i18n key="ESLC_LANGUAGE" />
        <b class="caret"></b>
    </span>
</a>
<ul class="dropdown-menu animated fadeInRight m-t-xs">
    <@wp.freemarkerTemplateParameter var="langsListVar" valueName="langsVar" removeOnEndTag=true >
    <#list langsListVar as curLangVar>
    <li <#if (curLangVar.code == currentLangVar)>class="active" </#if>>
        <a href="<@wp.url lang="${curLangVar.code}" paramRepeat=true />">
        <@wp.i18n key="ESLC_LANG_${curLangVar.code}" />
        </a>
    </li>
    </#list>
    </@wp.freemarkerTemplateParameter>
</ul>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entando-widget-login_form','entando-widget-login_form',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.headInfo type="JS" info="entando-misc-jquery/jquery-3.4.1.min.js" />
<@wp.headInfo type="JS" info="entando-misc-bootstrap/bootstrap.min.js" />

<ul class="nav pull-right">
	<li class="span2 dropdown
<#if (accountExpired?? && accountExpired == true) || (wrongAccountCredential?? && wrongAccountCredential == true)>open</#if>
">

	<#if (Session.currentUser != "guest")>
			<div class="btn-group">
				<button class="btn span2 text-left dropdown-toggle" data-toggle="dropdown">
					${Session.currentUser}
					<span class="caret pull-right"></span>
				</button>
				<ul class="dropdown-menu pull-right well-small">
					<li class="padding-medium-vertical">

						<@wp.ifauthorized permission="enterBackend">
						<p>
							<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/main.action?request_locale=<@wp.info key="currentLang" />"><span class="icon-wrench"></span> <@wp.i18n key="ESLF_ADMINISTRATION" /></a>
						</p>
						</@wp.ifauthorized>
						<div class="divider"></div>
						<p class="help-block text-right">
							<a class="btn" href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action"><@wp.i18n key="ESLF_SIGNOUT" /></a>
						</p>
						<@wp.pageWithWidget var="editProfilePageVar" widgetTypeCode="userprofile_editCurrentUser" />
						<#if (editProfilePageVar??) >
						<p class="help-block text-right">
							<a href="<@wp.url page="${editProfilePageVar.code}" />" ><@wp.i18n key="ESLF_PROFILE_CONFIGURATION" /></a>
						</p>
						</#if>
					</li>
				</ul>
			</div>
		<#else>
			<a class="dropdown-toggle text-right" data-toggle="dropdown" href="#"><@wp.i18n key="ESLF_SIGNIN" /> <span class="caret"></span></a>
			<ul class="dropdown-menu well-small">
				<li>
					<form class="form-vertical" method="POST">
						<#if (accountExpired?? && accountExpired == true)>
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert">x</button>
							<@wp.i18n key="ESLF_USER_STATUS_EXPIRED" />
						</div>
						</#if>
						<#if (wrongAccountCredential?? && wrongAccountCredential == true)>
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert">x</button>
							<@wp.i18n key="ESLF_USER_STATUS_CREDENTIALS_INVALID" />
						</div>
						</#if>
						<input type="text" name="username" class="input-large" placeholder="<@wp.i18n key="ESLF_USERNAME" />">
						<input type="password" name="password" class="input-large" placeholder="<@wp.i18n key="ESLF_PASSWORD" />">
						<p class="text-right">
							<input type="submit" class="btn btn-primary" value="<@wp.i18n key="ESLF_SIGNIN" />" />
						</p>
					</form>
				</li>
			</ul>
		</#if>
	</li>
</ul>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entando-widget-login_form_inspinia','entando-widget-login_form_inspinia',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<li class=" dropdown
    <#if (accountExpired?? && accountExpired == true) || (wrongAccountCredential?? && wrongAccountCredential == true)>open</#if> ">
    <#if (Session.currentUser != "guest")>
  
    <a class="btn  text-left dropdown-toggle" href="#" data-toggle="dropdown">
        ${Session.currentUser}
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li>
            <@wp.ifauthorized permission="enterBackend">
            <a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/main.action?request_locale=<@wp.info key="currentLang" />">
               <i class="fa fa-cube"></i>      
                <@wp.i18n key="ESLF_ADMINISTRATION" />
            </a>
            </@wp.ifauthorized>
        </li>
        <div class="divider"></div>
        <li> 
            <a class="btn" href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action">
               <i class="fa fa-sign-out"></i>           
                <@wp.i18n key="ESLF_SIGNOUT" />
            </a>
        </li>
        <@wp.pageWithWidget var="editProfilePageVar" widgetTypeCode="userprofile_editCurrentUser" />
        <#if (editProfilePageVar??) >
        <li>
            <a href="<@wp.url page="${editProfilePageVar.code}" />" ><@wp.i18n key="ESLF_PROFILE_CONFIGURATION" /></a>
        </li>
        </#if>
    </ul>
    <#else>
 
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <@wp.i18n key="ESLF_SIGNIN" />
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li>
            <form class="m-t" style="padding:10px;" method="POST">
                <#if (accountExpired?? && accountExpired == true)>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <@wp.i18n key="ESLF_USER_STATUS_EXPIRED" />
                </div>
                </#if>
                <#if (wrongAccountCredential?? && wrongAccountCredential == true)>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <@wp.i18n key="ESLF_USER_STATUS_CREDENTIALS_INVALID" />
                </div>
                </#if>
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="<@wp.i18n key="ESLF_USERNAME" />">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control"  placeholder="<@wp.i18n key="ESLF_PASSWORD" />">
                </div>
                <input type="submit" class="btn btn-primary block full-width m-b" value="<@wp.i18n key="ESLF_SIGNIN" />" />
            </form>
        </li>
    </ul>
    </#if>
</li>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entando-widget-navigation_bar','entando-widget-navigation_bar',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<@wp.headInfo type="JS" info="entando-misc-jquery/jquery-3.4.1.min.js" />
<@wp.headInfo type="JS" info="entando-misc-bootstrap/bootstrap.min.js" />

<@wp.currentPage param="code" var="currentPageCode" />
<@wp.freemarkerTemplateParameter var="currentPageCode" valueName="currentPageCode" />
<ul class="nav">
<@wp.nav var="page">

<#if (previousPage?? && previousPage.code??)>
	<#assign previousLevel=previousPage.level>
	<#assign level=page.level>
        <@wp.freemarkerTemplateParameter var="level" valueName="level" />
	<@wp.freemarkerTemplateParameter var="previousLevel" valueName="previousLevel" />
	<@wp.fragment code="entando-widget-navigation_bar_include" escapeXml=false />
</#if>

	<@wp.freemarkerTemplateParameter var="previousPage" valueName="page" />
</@wp.nav>

<#if (previousPage??)>
	<#assign previousLevel=previousPage.level>
        <#assign level=0>
	<@wp.freemarkerTemplateParameter var="level" valueName="level" />
	<@wp.freemarkerTemplateParameter var="previousLevel" valueName="previousLevel" />
	<@wp.fragment code="entando-widget-navigation_bar_include" escapeXml=false />

        <#if (previousLevel != 0)>
        <#list 0..(previousLevel - 1) as ignoreMe>
            </ul></li>
        </#list>

	</#if>
</#if>

</ul>
<@wp.freemarkerTemplateParameter var="previousPage" valueName="" removeOnEndTag=true />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entando-widget-navigation_bar_include',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>


<#assign liClass="">
<#assign homeIcon="">
<#assign caret="">
<#assign ulClass='' class="dropdown-menu"''>
<#assign aClassAndData="">
<#assign aURL=previousPage.url>

<#if (previousPage.voidPage)>
       <#assign aURL=''#'' />
</#if>

<#if (previousPage.code?contains("homepage"))>
     <#assign homeIcon=''<i class="icon-home"></i>&#32;''>
</#if>

<#if (previousPage.code == currentPageCode)>
     <#assign liClass='' class="active"''>
</#if>

<#if (previousLevel < level)>
    <#assign liClass='' class="dropdown"'' >

    <#if (previousPage.code == currentPageCode)>
	<#assign liClass='' class="dropdown active"''>
    </#if>

    <#if previousPage.voidPage>
	<#assign liClass='' class=" dropdown"'' >
    </#if>

    <#if (previousLevel > 0) >
	<#assign liClass='' class="dropdown-submenu"''>
	<#if (previousPage.code == currentPageCode)>
		<#assign liClass='' class="dropdown-submenu active"''>
    	</#if>

	<#assign ulClass='' class="dropdown-menu"''>
    </#if>

    <#assign aClassAndData='' class="dropdown-toggle" data-toggle="dropdown"''>

    <#if (previousLevel == 0)>
	<#assign caret='' <span class="caret"></span>''>
    </#if>
</#if>

<li ${liClass} >
	<a href="${aURL}"  ${aClassAndData} >
				<!-- [ ${previousLevel} ] -->
				${homeIcon}
				${previousPage.title}
				${caret}
	</a>

<#if (previousLevel == level)></li></#if>
<#if (previousLevel < level)>
    <ul ${ulClass}>
</#if>
<#if (previousLevel > level)>
     <#list 1..(previousLevel - level) as ignoreMe>
            </li></ul>
     </#list>
    </li>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entando-widget-navigation_bar_inspinia','entando-widget-navigation_bar_inspinia',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<@wp.currentPage param="code" var="currentPageCode" />
<@wp.freemarkerTemplateParameter var="currentPageCode" valueName="currentPageCode" />

<@wp.nav var="page">

<#if (previousPage?? && previousPage.code??)>
	<#assign previousLevel=previousPage.level>
	<#assign level=page.level>
        <@wp.freemarkerTemplateParameter var="level" valueName="level" />
	<@wp.freemarkerTemplateParameter var="previousLevel" valueName="previousLevel" />
	<@wp.fragment code="entando-widget-navigation_bar_inspinia_include" escapeXml=false />
</#if>

	<@wp.freemarkerTemplateParameter var="previousPage" valueName="page" />
</@wp.nav>

<#if (previousPage??)>
	<#assign previousLevel=previousPage.level>
        <#assign level=0>
	<@wp.freemarkerTemplateParameter var="level" valueName="level" />
	<@wp.freemarkerTemplateParameter var="previousLevel" valueName="previousLevel" />
	<@wp.fragment code="entando-widget-navigation_bar_inspinia_include" escapeXml=false />

        <#if (previousLevel != 0)>
        <#list 0..(previousLevel - 1) as ignoreMe>
            </ul></li>
        </#list>
                
	</#if>
</#if>

<@wp.freemarkerTemplateParameter var="previousPage" valueName="" removeOnEndTag=true />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entando-widget-navigation_bar_inspinia_include',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>


<#assign liClass="">
<#assign homeIcon="">
<#assign caret="">
<#assign ulClass='' class="dropdown-menu"''>
<#assign aClassAndData="">
<#assign aURL=previousPage.url>

<#if (previousPage.voidPage)>
       <#assign aURL=''#'' />
</#if>

<#if (previousPage.code?contains("homepage"))>
     <#assign homeIcon=''<i class="icon-home"></i>&#32;''>
</#if>

<#if (previousPage.code == currentPageCode)>
     <#assign liClass='' class="active"''>
</#if>

<#if (previousLevel < level)>
    <#assign liClass='' class="dropdown"'' >

    <#if (previousPage.code == currentPageCode)>
	<#assign liClass='' class="dropdown active"''>
    </#if>

    <#if previousPage.voidPage>
	<#assign liClass='' class=" dropdown"'' >
    </#if>

    <#if (previousLevel > 0) >
	<#assign liClass='' class="dropdown-submenu"''>
	<#if (previousPage.code == currentPageCode)>
		<#assign liClass='' class="dropdown-submenu active"''>
    	</#if>

	<#assign ulClass='' class="dropdown-menu"''>
    </#if>

    <#assign aClassAndData='' class="dropdown-toggle" data-toggle="dropdown"''>

    <#if (previousLevel == 0)>
	<#assign caret='' <span class="caret"></span>''>
    </#if>
</#if>

<li ${liClass} > 
	<a href="${aURL}"  ${aClassAndData} >
				<!-- [ ${previousLevel} ] -->
				${homeIcon}
				${previousPage.title}
				${caret}
	</a>

<#if (previousLevel == level)></li></#if>
<#if (previousLevel < level)>
    <ul ${ulClass}>
</#if>
<#if (previousLevel > level)>
     <#list 1..(previousLevel - level) as ignoreMe>
            </li></ul>
     </#list>
    </li>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entando-widget-search_form','entando-widget-search_form',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.pageWithWidget var="searchResultPageVar" widgetTypeCode="search_result" listResult=false />
<form class="navbar-search pull-left" action="<#if (searchResultPageVar??) ><@wp.url page="${searchResultPageVar.code}" /></#if>" method="get">
<input type="text" name="search" class="search-query span2" placeholder="<@wp.i18n key="ESSF_SEARCH" />" x-webkit-speech="x-webkit-speech" />
</form>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entandoapi_is_resource_detail','entando_apis',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@s.set var="apiResourceVar" value="apiResource" />
<@s.set var="GETMethodVar" value="#apiResourceVar.getMethod" />
<@s.set var="POSTMethodVar" value="#apiResourceVar.postMethod" />
<@s.set var="PUTMethodVar" value="#apiResourceVar.putMethod" />
<@s.set var="DELETEMethodVar" value="#apiResourceVar.deleteMethod" />
<@s.set var="apiNameVar" value="(#apiResourceVar.namespace!=null && #apiResourceVar.namespace.length()>0 ? ''/'' + #apiResourceVar.namespace : '''')+''/''+#apiResourceVar.resourceName" />
<section>
<p>
	<a href="<@wp.action path="/ExtStr2/do/Front/Api/Resource/list.action" />" class="btn btn-primary"><i class="icon-arrow-left icon-white"></i>&#32;<@wp.i18n key="ENTANDO_API_GOTO_LIST" /></a>
</p>
<h2><@wp.i18n key="ENTANDO_API_RESOURCE" />&#32;<@s.property value="#apiNameVar" /></h2>
<@s.if test="hasActionMessages()">
	<div class="alert alert-box alert-success">
		<h3 class="alert-heading"><@wp.i18n key="ENTANDO_API_ERROR" /></h3>
		<ul>
			<@s.iterator value="actionMessages">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.if test="hasActionErrors()">
	<div class="alert alert-box alert-error">
		<h3 class="alert-heading"><@wp.i18n key="ENTANDO_API_ERROR" /></h3>
		<ul>
			<@s.iterator value="actionErrors">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<!-- DESCRIPTION -->
<p><@s.property value="#apiResourceVar.description" /></p>

<!-- INFO -->
<dl class="dl-horizontal">
	<dt><@wp.i18n key="ENTANDO_API_RESOURCE_NAME" /></dt>
		<dd><@s.property value="#apiResourceVar.resourceName" /></dd>
	<dt><span lang="en"><@wp.i18n key="ENTANDO_API_RESOURCE_NAMESPACE" /></span></dt>
		<dd>/<@s.property value="#apiResourceVar.namespace" /></dd>
	<dt><@wp.i18n key="ENTANDO_API_RESOURCE_SOURCE" /></dt>
		<dd>
			<@s.property value="#apiResourceVar.source" /><@s.if test="%{#apiResourceVar.pluginCode != null && #apiResourceVar.pluginCode.length() > 0}">,&#32;<@s.property value="%{getText(#apiResourceVar.pluginCode+''.name'')}" />&#32;(<@s.property value="%{#apiResourceVar.pluginCode}" />)</@s.if>
		</dd>
	<dt><@wp.i18n key="ENTANDO_API_RESOURCE_URI" /></dt>
		<dd>
			<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />legacyapi/rs/<@wp.info key="currentLang" /><@s.if test="null != #apiResourceVar.namespace">/<@s.property value="#apiResourceVar.namespace" /></@s.if>/<@s.property value="#apiResourceVar.resourceName" />"><@wp.info key="systemParam" paramName="applicationBaseURL" />legacyapi/rs/<@wp.info key="currentLang" /><@s.if test="null != #apiResourceVar.namespace">/<@s.property value="#apiResourceVar.namespace" /></@s.if>/<@s.property value="#apiResourceVar.resourceName" /></a>
		</dd>
	<dt>
		<@wp.i18n key="ENTANDO_API_EXTENSION" />
	</dt>
		<dd>
			<@wp.i18n key="ENTANDO_API_EXTENSION_NOTE" />
		</dd>
</dl>

	<@s.set var="methodVar" value="#GETMethodVar" />
	<@s.set var="currentMethodNameVar" value="%{''GET''}" />
	<h3 id="api_method_GET">GET</h3>
	<@wp.fragment code="entandoapi_is_resource_detail_include" escapeXml=false />

	<@s.set var="methodVar" value="#POSTMethodVar" />
	<@s.set var="currentMethodNameVar" value="%{''POST''}" />
	<h3 id="api_method_POST">POST</h3>
	<@wp.fragment code="entandoapi_is_resource_detail_include" escapeXml=false />

	<@s.set var="methodVar" value="#PUTMethodVar" />
	<@s.set var="currentMethodNameVar" value="%{''PUT''}" />
	<h3 id="api_method_PUT">PUT</h3>
	<@wp.fragment code="entandoapi_is_resource_detail_include" escapeXml=false />

	<@s.set var="methodVar" value="#DELETEMethodVar" />
	<@s.set var="currentMethodNameVar" value="%{''DELETE''}" />
	<h3 id="api_method_DELETE">DELETE</h3>
	<@wp.fragment code="entandoapi_is_resource_detail_include" escapeXml=false />
<p>
	<a href="<@wp.action path="/ExtStr2/do/Front/Api/Resource/list.action" />" class="btn btn-primary"><i class="icon-arrow-left icon-white"></i>&#32;<@wp.i18n key="ENTANDO_API_GOTO_LIST" /></a>
</p>
</section>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entandoapi_is_resource_detail_include',NULL,NULL,'','<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@s.if test="#methodVar == null">
	<p>
		<@s.property value="#currentMethodNameVar" />,&#32;<@wp.i18n key="ENTANDO_API_METHOD_KO" />
	</p>
</@s.if>
<@s.else>
	<dl class="dl-horizontal">
		<dt>
			<@wp.i18n key="ENTANDO_API_METHOD" />
		</dt>
			<dd>
				<@wp.i18n key="ENTANDO_API_METHOD_OK" />
			</dd>
		<@s.if test="#methodVar != null">
			<dt>
				<@wp.i18n key="ENTANDO_API_DESCRIPTION" />
			</dt>
				<dd><@s.property value="#methodVar.description" /></dd>
			<dt>
				<@wp.i18n key="ENTANDO_API_METHOD_AUTHORIZATION" />
			</dt>
				<dd>
					<@s.if test="%{null != #methodVar.requiredPermission}">
						<@s.iterator value="methodAuthorityOptions" var="permission"><@s.if test="#permission.key==#methodVar.requiredPermission"><@s.property value="#permission.value" /></@s.if></@s.iterator>
					</@s.if>
					<@s.elseif test="%{#methodVar.requiredAuth}">
						<@wp.i18n key="ENTANDO_API_METHOD_AUTH_SIMPLE" />
					</@s.elseif>
					<@s.else>
						<@wp.i18n key="ENTANDO_API_METHOD_AUTH_FREE" />
					</@s.else>
				</dd>
			<@s.if test=''%{!#methodVar.resourceName.equalsIgnoreCase("getService")}'' >
			<dt>
				<@wp.i18n key="ENTANDO_API_METHOD_SCHEMAS" />
			</dt>
				<dd class="schemas">
					<@s.if test=''%{#methodVar.httpMethod.toString().equalsIgnoreCase("POST") || #methodVar.httpMethod.toString().equalsIgnoreCase("PUT")}''>
						<@wp.action path="/ExtStr2/do/Front/Api/Resource/requestSchema.action" var="requestSchemaURLVar" >
							<@wp.parameter name="resourceName"><@s.property value="#methodVar.resourceName" /></@wp.parameter>
							<@wp.parameter name="namespace"><@s.property value="#methodVar.namespace" /></@wp.parameter>
							<@wp.parameter name="httpMethod"><@s.property value="#methodVar.httpMethod" /></@wp.parameter>
						</@wp.action>
						<a href="${requestSchemaURLVar}" >
							<@wp.i18n key="ENTANDO_API_METHOD_SCHEMA_REQ" />
						</a>
						<br />
					</@s.if>
						<@wp.action path="/ExtStr2/do/Front/Api/Resource/responseSchema.action" var="responseSchemaURLVar" >
							<@wp.parameter name="resourceName"><@s.property value="#methodVar.resourceName" /></@wp.parameter>
							<@wp.parameter name="namespace"><@s.property value="#methodVar.namespace" /></@wp.parameter>
							<@wp.parameter name="httpMethod"><@s.property value="#methodVar.httpMethod" /></@wp.parameter>
						</@wp.action>
						<a href="${responseSchemaURLVar}" >
							<@wp.i18n key="ENTANDO_API_METHOD_SCHEMA_RESP" />
						</a>
				</dd>
			</@s.if>
		</@s.if>
	</dl>
	<@s.if test="#methodVar != null">
		<@s.set var="methodParametersVar" value="#methodVar.parameters" />
		<@s.if test="null != #methodParametersVar && #methodParametersVar.size() > 0">
			<table class="table table-striped table-bordered table-condensed">
				<caption><@wp.i18n key="ENTANDO_API_METHOD_REQUEST_PARAMS" /></caption>
				<tr>
					<th><@wp.i18n key="ENTANDO_API_PARAM_NAME" /></th>
					<th><@wp.i18n key="ENTANDO_API_PARAM_DESCRIPTION" /></th>
					<th><@wp.i18n key="ENTANDO_API_PARAM_REQUIRED" /></th>
				</tr>
				<@s.iterator value="#methodParametersVar" var="apiParameter" >
					<tr>
						<td><@s.property value="#apiParameter.key" /></td>
						<td><@s.property value="#apiParameter.description" /></td>
						<td class="icon required_<@s.property value="#apiParameter.required" />">
							<@s.if test="#apiParameter.required">
								<@wp.i18n key="YES" />
							</@s.if>
							<@s.else>
								<@wp.i18n key="NO" />
							</@s.else>
						</td>
					</tr>
				</@s.iterator>
			</table>
		</@s.if>
	</@s.if>
</@s.else>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entandoapi_is_resource_list','entando_apis',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<h2><@wp.i18n key="ENTANDO_API_RESOURCES" /></h2>
<@s.if test="hasActionErrors()">
	<div class="alert alert-block alert-error">
		<h3 class="alert-heading"><@wp.i18n key="ENTANDO_API_ERROR" /></h3>
		<ul>
			<@s.iterator value="actionErrors">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.set var="resourceFlavoursVar" value="resourceFlavours" />

<@s.if test="#resourceFlavoursVar.size() > 0">
	<@s.set var="icon_free"><span class="icon icon-ok"></span><span class="noscreen sr-only"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_FREE" /></span></@s.set>
	<@s.set var="title_free"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_FREE" />. <@wp.i18n key="ENTANDO_API_GOTO_DETAILS" /></@s.set>

	<@s.set var="icon_auth"><span class="icon icon-user"></span><span class="noscreen sr-only"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_AUTH" /></span></@s.set>
	<@s.set var="title_auth"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_AUTH" />. <@wp.i18n key="ENTANDO_API_GOTO_DETAILS" /></@s.set>

	<@s.set var="icon_lock"><span class="icon icon-lock"></span><span class="noscreen sr-only"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_LOCK" /></span></@s.set>
	<@s.set var="title_lock"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_LOCK" />. <@wp.i18n key="ENTANDO_API_GOTO_DETAILS" /></@s.set>

	<@s.iterator var="resourceFlavourVar" value="#resourceFlavoursVar" status="resourceFlavourStatusVar">
		<table class="table table-striped table-bordered table-condensed">
			<@s.iterator value="#resourceFlavourVar" var="resourceVar" status="statusVar" >
				<@s.if test="#statusVar.first">
					<@s.if test="#resourceVar.source==''core''"><@s.set var="captionVar"><@s.property value="#resourceVar.source" escapeHtml=false /></@s.set></@s.if>
					<@s.else><@s.set var="captionVar"><@s.property value="%{getText(#resourceVar.sectionCode+''.name'')}" escapeHtml=false /></@s.set></@s.else>
					<caption>
						<@s.property value="#captionVar" />
					</caption>
					<tr>
						<th class="span3"><@wp.i18n key="ENTANDO_API_RESOURCE" /></th>
						<th><@wp.i18n key="ENTANDO_API_DESCRIPTION" /></th>
						<th class="text-center span1">GET</th>
						<th class="text-center span1">POST</th>
						<th class="text-center span1">PUT</th>
						<th class="text-center span1">DELETE</th>
					</tr>
				</@s.if>
				<tr>
					<td>
						<@wp.action path="/ExtStr2/do/Front/Api/Resource/detail.action" var="detailActionURL">
							<@wp.parameter name="resourceName"><@s.property value="#resourceVar.resourceName" /></@wp.parameter>
							<@wp.parameter name="namespace"><@s.property value="#resourceVar.namespace" /></@wp.parameter>
						</@wp.action>
						<a title="<@wp.i18n key="ENTANDO_API_GOTO_DETAILS" />:&#32;/<@s.property value="%{#resourceVar.namespace.length()>0?#resourceVar.namespace+''/'':''''}" /><@s.property value="#resourceVar.resourceName" />" href="${detailActionURL}" ><@s.property value="#resourceVar.resourceName" /></a>
					</td>
					<td><@s.property value="#resourceVar.description" /></td>
					<td class="text-center">
						<@s.if test="#resourceVar.getMethod != null && #resourceVar.getMethod.active && (!#resourceVar.getMethod.hidden)" >
							<@s.if test="#resourceVar.getMethod.requiredPermission != null" ><@s.set var="icon" value="#icon_lock" /><@s.set var="title" value="#title_lock" /></@s.if>
							<@s.elseif test="#resourceVar.getMethod.requiredAuth" ><@s.set var="icon" value="#icon_auth" /><@s.set var="title" value="#title_auth" /></@s.elseif>
							<@s.else><@s.set var="icon" value="#icon_free" /><@s.set var="title" value="#title_free" /></@s.else>
							<a href="${detailActionURL}#api_method_GET" title="<@s.property value="#title" />">
								<@s.property value="#icon" escapeHtml=false />
							</a>
						</@s.if>
						<@s.else><abbr title="<@wp.i18n key="ENTANDO_API_METHOD_STATUS_NA" />">&ndash;</abbr></@s.else>
					</td>
					<td class="text-center">
						<@s.if test="#resourceVar.postMethod != null && #resourceVar.postMethod.active && (!#resourceVar.postMethod.hidden)" >
							<@s.if test="#resourceVar.postMethod.requiredPermission != null" ><@s.set var="icon" value="#icon_lock" /><@s.set var="title" value="#title_lock" /></@s.if>
							<@s.elseif test="#resourceVar.postMethod.requiredAuth" ><@s.set var="icon" value="#icon_auth" /><@s.set var="title" value="#title_auth" /></@s.elseif>
							<@s.else><@s.set var="icon" value="#icon_free" /><@s.set var="title" value="#title_free" /></@s.else>
							<a href="${detailActionURL}#api_method_POST" title="<@s.property value="#title" />">
								<@s.property value="#icon" escapeHtml=false />
							</a>
						</@s.if>
						<@s.else><abbr title="<@wp.i18n key="ENTANDO_API_METHOD_STATUS_NA" />">&ndash;</abbr></@s.else>
					</td>
					<td class="text-center">
						<@s.if test="#resourceVar.putMethod != null && #resourceVar.putMethod.active && (!#resourceVar.putMethod.hidden)" >
							<@s.if test="#resourceVar.putMethod.requiredPermission != null" ><@s.set var="icon" value="#icon_lock" /><@s.set var="title" value="#title_lock" /></@s.if>
							<@s.elseif test="#resourceVar.putMethod.requiredAuth" ><@s.set var="icon" value="#icon_auth" /><@s.set var="title" value="#title_auth" /></@s.elseif>
							<@s.else><@s.set var="icon" value="#icon_free" /><@s.set var="title" value="#title_free" /></@s.else>
							<a href="${detailActionURL}#api_method_PUT" title="<@s.property value="#title" />">
								<@s.property value="#icon" escapeHtml=false />
							</a>
						</@s.if>
						<@s.else><abbr title="<@wp.i18n key="ENTANDO_API_METHOD_STATUS_NA" />">&ndash;</abbr></@s.else>
					</td>
					<td class="text-center">
						<@s.if test="#resourceVar.deleteMethod != null && #resourceVar.deleteMethod.active && (!#resourceVar.deleteMethod.hidden)" >
							<@s.if test="#resourceVar.deleteMethod.requiredPermission != null" ><@s.set var="icon" value="#icon_lock" /><@s.set var="title" value="#title_lock" /></@s.if>
							<@s.elseif test="#resourceVar.deleteMethod.requiredAuth" ><@s.set var="icon" value="#icon_auth" /><@s.set var="title" value="#title_auth" /></@s.elseif>
							<@s.else><@s.set var="icon" value="#icon_free" /><@s.set var="title" value="#title_free" /></@s.else>
							<a href="${detailActionURL}#api_method_DELETE" title="<@s.property value="#title" />">
								<@s.property value="#icon" escapeHtml=false />
							</a>
						</@s.if>
						<@s.else><abbr title="<@wp.i18n key="ENTANDO_API_METHOD_STATUS_NA" />">&ndash;</abbr></@s.else>
					</td>
				</tr>
			</@s.iterator>
		</table>

		<@s.if test="#resourceVar.source==''core''">
			<a href="<@wp.action path="/ExtStr2/do/Front/Api/Service/list.action" />" class="btn btn-primary pull-right"><@wp.i18n key="ENTANDO_API_GOTO_SERVICE_LIST" /></a>
		</@s.if>
	</@s.iterator>
</@s.if>
<@s.else>
	<p><@wp.i18n key="ENTANDO_API_NO_RESOURCES" /></p>
</@s.else>
<script>
  $(function () {
    $(''#api-togglers a:first'').tab(''show'');
  })
</script>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entandoapi_is_service_detail','entando_apis',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@wp.headInfo type="CSS" info="widgets/api.css"/>
<@s.set var="apiServiceVar" value="%{getApiService(serviceKey)}" />
<div class="entando-api api-resource-detail">
<h2><@wp.i18n key="ENTANDO_API_SERVICE" />&#32;<@s.property value="serviceKey" /></h2>
<@s.if test="hasActionMessages()">
	<div class="message message_confirm">
		<h3><@wp.i18n key="ENTANDO_API_ERROR" /></h3>
		<ul>
			<@s.iterator value="actionMessages">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.if test="hasActionErrors()">
	<div class="message message_error">
		<h3><@wp.i18n key="ENTANDO_API_ERROR" /></h3>
		<ul>
			<@s.iterator value="actionErrors">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>

<p class="description"><@s.property value="getTitle(serviceKey, #apiServiceVar.description)" /></p>

<@s.set var="masterMethodVar" value="#apiServiceVar.master" />

<dl class="dl-horizontal">
	<dt><@wp.i18n key="ENTANDO_API_SERVICE_KEY" /></dt>
		<dd><@s.property value="serviceKey" /></dd>
	<dt><@wp.i18n key="ENTANDO_API_SERVICE_PARENT_API" /></dt>
		<dd><@s.property value="#masterMethodVar.description" />&#32;(/<@s.if test="#masterMethodVar.namespace!=null && #masterMethodVar.namespace.length()>0"><@s.property value="#masterMethodVar.namespace" />/</@s.if><@s.property value="#masterMethodVar.resourceName" />)</dd>
	<dt>
		<@wp.i18n key="ENTANDO_API_SERVICE_AUTHORIZATION" />
	</dt>
		<dd>
			<@s.if test="%{!#apiServiceVar.requiredAuth}" >
				<@wp.i18n key="ENTANDO_API_SERVICE_AUTH_FREE" />
			</@s.if>
			<@s.elseif test="%{null == #apiServiceVar.requiredPermission && null == #apiServiceVar.requiredGroup}">
				<@wp.i18n key="ENTANDO_API_SERVICE_AUTH_SIMPLE" />
			</@s.elseif>
			<@s.else>
				<@s.set var="serviceAuthGroupVar" value="%{getGroup(#apiServiceVar.requiredGroup)}" />
				<@s.set var="serviceAuthPermissionVar" value="%{getPermission(#apiServiceVar.requiredPermission)}" />
				<@s.if test="%{null != #serviceAuthPermissionVar}">
					<@wp.i18n key="ENTANDO_API_SERVICE_AUTH_WITH_PERM" />&#32;<@s.property value="#serviceAuthPermissionVar.description" />
				</@s.if>
				<@s.if test="%{null != #serviceAuthGroupVar}">
					<@s.if test="%{null != #serviceAuthPermissionVar}"><br /></@s.if>
					<@wp.i18n key="ENTANDO_API_SERVICE_AUTH_WITH_GROUP" />&#32;<@s.property value="#serviceAuthGroupVar.descr" />
				</@s.if>
			</@s.else>
		</dd>
	<dt><@wp.i18n key="ENTANDO_API_SERVICE_URI" /></dt>
		<dd>
			<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />legacyapi/rs/<@wp.info key="currentLang" />/getService?key=<@s.property value="serviceKey" />"><@wp.info key="systemParam" paramName="applicationBaseURL" />legacyapi/rs/<@wp.info key="currentLang" />/getService?key=<@s.property value="serviceKey" /></a>
		</dd>
	<dt>
		<@wp.i18n key="ENTANDO_API_EXTENSION" />
	</dt>
		<dd>
			<@wp.i18n key="ENTANDO_API_EXTENSION_NOTE" />
		</dd>
	<dt>
		<@wp.i18n key="ENTANDO_API_SERVICE_SCHEMAS" />
	</dt>
		<dd class="schemas">
			<@wp.action path="/ExtStr2/do/Front/Api/Service/responseSchema.action" var="responseSchemaURLVar" >
				<@wp.parameter name="serviceKey"><@s.property value="serviceKey" /></@wp.parameter>
			</@wp.action>
			<a href="${responseSchemaURLVar}" >
				<@wp.i18n key="ENTANDO_API_SERVICE_SCHEMA_RESP" />
			</a>
		</dd>
</dl>

<@s.if test="%{null != #apiServiceVar.freeParameters && #apiServiceVar.freeParameters.length > 0}" >
<table class="table table-striped table-bordered table-condensed" summary="<@wp.i18n key="ENTANDO_API_SERVICE_PARAMETERS_SUMMARY" />">
	<caption><span><@wp.i18n key="ENTANDO_API_SERVICE_PARAMETERS" /></span></caption>
	<tr>
		<th><@wp.i18n key="ENTANDO_API_SERVICE_PARAM_NAME" /></th>
		<th><@wp.i18n key="ENTANDO_API_SERVICE_PARAM_DESCRIPTION" /></th>
		<th><@wp.i18n key="ENTANDO_API_SERVICE_PARAM_REQUIRED" /></th>
		<th><@wp.i18n key="ENTANDO_API_SERVICE_PARAM_DEFAULT_VALUE" /></th>
	</tr>
	<@s.iterator value="#apiServiceVar.freeParameters" var="apiParameterNameVar" >
		<@s.set var="apiParameterValueVar" value="%{#apiServiceVar.parameters[#apiParameterNameVar]}" />
		<@s.set var="apiParameterVar" value="%{#apiServiceVar.master.getParameter(#apiParameterNameVar)}" />
		<@s.set var="apiParameterRequiredVar" value="%{#apiParameterVar.required && null == #apiParameterValueVar}" />
		<tr>
			<td><label for="<@s.property value="#apiParameterNameVar" />"><@s.property value="#apiParameterNameVar" /></label></td>
			<td><@s.property value="%{#apiParameterVar.description}" /></td>
			<td class="icon required_<@s.property value="#apiParameterRequiredVar" />">
				<@s.if test="#apiParameterRequiredVar" ><@wp.i18n key="YES" /></@s.if>
				<@s.else><@wp.i18n key="NO" /></@s.else>
			</td>
			<td><@s.if test="null != #apiParameterValueVar"><@s.property value="#apiParameterValueVar" /></@s.if><@s.else>-</@s.else></td>
		</tr>
	</@s.iterator>
</table>
</@s.if>
<p class="api-back">
	<a class="btn btn-primary" href="<@wp.action path="/ExtStr2/do/Front/Api/Resource/list.action" />"><span class="icon-arrow-left icon-white"></span>&#32;<@wp.i18n key="ENTANDO_API_GOTO_LIST" /></a>
</p>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entandoapi_is_service_list','entando_apis',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<section>

<p>
	<a href="<@wp.action path="/ExtStr2/do/Front/Api/Resource/list.action" />" class="btn btn-primary"><i class="icon-arrow-left icon-white"></i>&#32;<@wp.i18n key="ENTANDO_API_GOTO_LIST" /></a>
</p>

<h2><@wp.i18n key="ENTANDO_API_GOTO_SERVICE_LIST" /></h2>
<@s.if test="hasActionErrors()">
	<div class="alert alert-block alert-error">
		<h3 class="alert-heading"><@s.text name="message.title.ActionErrors" /></h3>
		<ul>
			<@s.iterator value="actionErrors">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.if test="hasFieldErrors()">
	<div class="alert alert-block alert-error">
		<h3 class="alert-heading"><@s.text name="message.title.FieldErrors" /></h3>
		<ul>
			<@s.iterator value="fieldErrors">
				<@s.iterator value="value">
				<li><@s.property escapeHtml=false /></li>
				</@s.iterator>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.if test="hasActionMessages()">
	<div class="alert alert-block alert-info">
		<h3 class="alert-heading"><@s.text name="messages.confirm" /></h3>
		<ul>
			<@s.iterator value="actionMessages">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.set var="resourceFlavoursVar" value="resourceFlavours" />
<@s.set var="serviceFlavoursVar" value="serviceFlavours" />

<@s.if test="#serviceFlavoursVar != null && #serviceFlavoursVar.size() > 0">
<div class="tabbable tabs-left">
	<ul class="nav nav-tabs">
		<@s.iterator var="resourceFlavour" value="#resourceFlavoursVar" status="statusVar">
			<@s.set var="serviceGroupVar" value="#resourceFlavour.get(0).getSectionCode()" />
			<@s.set var="servicesByGroupVar" value="#serviceFlavoursVar[#serviceGroupVar]" />
			<@s.if test="null != #servicesByGroupVar && #servicesByGroupVar.size() > 0">
				<@s.if test="#serviceGroupVar == ''core''"><@s.set var="captionVar" value="%{#serviceGroupVar}" /></@s.if>
				<@s.else><@s.set var="captionVar" value="%{getText(#serviceGroupVar + ''.name'')}" /></@s.else>
				<li<@s.if test="#statusVar.first"> class="active"</@s.if>>
					<a href="#api-flavour-<@s.property value=''%{#captionVar.toLowerCase().replaceAll("[^a-z0-9-]", "")}'' />" data-toggle="tab"><@s.property value=''%{#captionVar}'' /></a>
				</li>
			</@s.if>
		</@s.iterator>
	</ul>

  <div class="tab-content">
	<@s.iterator var="resourceFlavour" value="#resourceFlavoursVar" status="moreStatusVar">
		<@s.set var="serviceGroupVar" value="#resourceFlavour.get(0).getSectionCode()" />
		<@s.set var="servicesByGroupVar" value="#serviceFlavoursVar[#serviceGroupVar]" />
		<@s.if test="null != #servicesByGroupVar && #servicesByGroupVar.size() > 0">
			<@s.if test="#serviceGroupVar == ''core''"><@s.set var="captionVar" value="%{#serviceGroupVar}" /></@s.if>
			<@s.else><@s.set var="captionVar" value="%{getText(#serviceGroupVar + ''.name'')}" /></@s.else>
			<div class="tab-pane<@s.if test="#moreStatusVar.first"> active</@s.if>" id="api-flavour-<@s.property value=''%{#captionVar.toLowerCase().replaceAll("[^a-z0-9]", "")}'' />">
			<table class="table table-striped table-bordered table-condensed">
				<caption>
					<@s.property value="#captionVar" />
				</caption>
				<tr>
					<th><@wp.i18n key="ENTANDO_API_SERVICE" /></th>
					<th><@wp.i18n key="ENTANDO_API_DESCRIPTION" /></th>
				</tr>
				<@s.iterator var="serviceVar" value="#servicesByGroupVar" >
					<tr>
						<td class="monospace">
							<@wp.action path="/ExtStr2/do/Front/Api/Service/detail.action" var="detailActionURL">
								<@wp.parameter name="serviceKey"><@s.property value="#serviceVar.key" /></@wp.parameter>
							</@wp.action>
							<a href="${detailActionURL}"><@s.property value="#serviceVar.key" /></a>
						</td>
						<td><@s.property value="#serviceVar.value" /></td>
					</tr>
				</@s.iterator>
			</table>
			</div>
		</@s.if>
	</@s.iterator>
	</div>
</div>
</@s.if>
<@s.else>
<div class="alert alert-block alert-info">
	<p><@wp.i18n key="ENTANDO_API_NO_SERVICES" escapeXml=false /></p>
</div>
</@s.else>

<p>
	<a href="<@wp.action path="/ExtStr2/do/Front/Api/Resource/list.action" />" class="btn btn-primary"><i class="icon-arrow-left icon-white"></i>&#32;<@wp.i18n key="ENTANDO_API_GOTO_LIST" /></a>
</p>

</section>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('esonero_funzioni','esonero_funzioni',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Argomenti e dettagli correlati</i></b></h4>
<hr>
<span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">
<p align="justify"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"><strong><font color="#006400">Ufficiali Giudici militari </font></strong></span></p>
<p align="left"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"></span><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"><em>Casistica sulle ipotesi di non immissione nell�esercizio delle funzioni giudiziarie</em></span></p>
<p class="MsoNormal" style="MARGIN: 0cm 0cm 10pt" align="justify"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"></span><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">Il Consiglio della Magistratura Militare ha ritenuto applicabile agli ufficiali estratti a sorte a comporre gli organi giudicanti della magistratura militare <span style="mso-no-proof: yes">il requisito dello �aver sempre tenuto illibata condotta civile e morale� (art. 8 R.D. 30 gennaio 1941 n. 12), </span>ai fini dell�immissione nell�esercizio delle funzioni.</span></p>
<p class="MsoNormal" style="MARGIN: 0cm 0cm 10pt" align="justify"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"></span><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">A seguito dell�abrogazione dell�art. 8, RD 12/1941, ad opera dell�art. 54 del Dlgs 5 aprile 2006, n. 160, il Consiglio della Magistratura Militare, con deliberazione n. 3168 del 13 gennaio 2009, ha ritenuto applicabile ai giudici militari la norma contenuta nell�art. 2, comma 2, lettera b-bis del Dlgs 160/2006, (inserita dall�articolo 1 della legge 30 luglio 2007, n. 111) che annovera tra le condizioni che devono essere soddisfatte per l�ammissione al concorso per esami per l�accesso in magistratura l� �essere di condotta incensurabile�.</span></p>
<p class="MsoNormal" style="MARGIN: 0cm 0cm 10pt" align="justify"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"></span><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">Nel documento allegato viene riportata in modo cronologico l�attivit� deliberativa del Consiglio della Magistratura Militare con riferimento ai provvedimenti adottati a decorrere dall�anno 2000.</span></p>
<p class="MsoNormal" style="MARGIN: 0cm 0cm 10pt" align="justify"><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%"></span><span style="FONT-SIZE: 10pt; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; LINE-HEIGHT: 115%">Allegato n. 1 (in formato compresso): <a <a href="/SIGMIL/reosurces/allegati/Casi_esclusione_Ufficiali_Giudici_militari.rar"  target="_blank"><strong><font color="#4682b4">Giudici militari - Casistica di esclusione dalle funzioni giudiziarie</font></strong></a></span></p></span></span></span>
 
 ',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('giustizia_mil_ita','giustizia_mil_ita',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Argomenti e dettagli correlati</i></b></h4>
<hr>
<div id="mainAccordion">
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Assetto della Giustizia Militare</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                  <a title="Organigramma" href="<@wp.url page=''organigramma''/>">Organigramma</a>
               </h5>
         </div>
      </div>
   </div>
	<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                  <a title="Cenni storici" href="<@wp.url page=''cenni_storici''/>">Cenni storici</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Ufficiali Giudici militari" href="<@wp.url page=''ufficiali_giudici_militari''/>">Ufficiali Giudici militari</a><br>Funzioni giudiziarie
               </h5>
         </div>
      </div>
   </div>
</div>
               ',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('header_global',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Stato Maggiore Difesa - Portale Archimede</title>
     <link rel="stylesheet"  href="<@wp.resourceURL/>static/libs/css/bootstrap.min.css" />
    <link rel="stylesheet"  href="<@wp.resourceURL/>static/css/archimede.css" />

	    <link rel="stylesheet" type="text/css" href="<@wp.resourceURL/>static/css/fontawesome-all.css" />
    <link rel="stylesheet" type="text/css" href="<@wp.resourceURL/>static/css/titillium-font.css" />
    <link rel="stylesheet" type="text/css" href="<@wp.resourceURL/>static/csr-assets/style.css" />
    <link rel="stylesheet" type="text/css" href="<@wp.resourceURL/>static/libs/js/jquery-ui/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="<@wp.resourceURL/>static/css/custom-fonts/css/fontello.css" />

    <link rel="stylesheet"  href="<@wp.resourceURL/>static/css/contacts.css" />
    <link rel="stylesheet"  href="<@wp.resourceURL/>static/css/hamburger.css" />
    <link rel="stylesheet"  href="<@wp.resourceURL/>static/css/innermenu.css" />
    <link rel="stylesheet"  href="<@wp.resourceURL/>static/libs/slick/slick.css" />
    <link rel="stylesheet"  href="<@wp.resourceURL/>static/libs/slick/slick-theme.css" />
 <link rel="stylesheet" type="text/css" href="<@wp.resourceURL/>static/css/animate.css" />
    <link rel="icon" href="<@wp.resourceURL/>static/img/logo_repubblica_ico.ico" />
    
    <script type="text/javascript" src="<@wp.resourceURL/>static/libs/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<@wp.resourceURL/>static/libs/js/popper.min.js"></script>
<script type="text/javascript" src="<@wp.resourceURL/>static/libs/js/bootstrap.js"></script>
   <script type="text/javascript" src="<@wp.resourceURL/>static/libs/slick/slick.min.js"></script>
   <script type="text/javascript" src="<@wp.resourceURL/>static/libs/js/jquery-ui/jquery-ui.min.js"></script>
   <script type="text/javascript" src="<@wp.resourceURL/>static/libs/js/jquery-it.js"></script>
<script type="text/javascript" src="<@wp.resourceURL/>static/js/contacts.js"></script>
    <script type="text/javascript" src="<@wp.resourceURL/>static/js/archimede.js"></script>
    <script type="text/javascript" src="<@wp.resourceURL/>static/js/hamburger.js"></script>
	<link rel="stylesheet" type="text/css" href="<@wp.resourceURL/>static/css/ie.css" /> 

<script>
$.datepicker.setDefaults($.datepicker.regional[''it'']);
</script>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('home_card','home_card',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 

<div id="mainTitle">
   <h2 class="border-bottom-blue pb-2">Magistratura militare</h2>
</div>
<div class="row">
<div class="col-6 p-1 magmil-area">
	<a href="<@wp.url page=''comuinicazioni_del_cmm_home''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/966da5dcabb4b9c478e60f5c1f66b1ca_d0.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Comunicazioni del CMM</h5>
      <span style="font-size: 0.85rem;">Per il personale della Giustizia Militare</span>
  	</div>
  </div>
  </a>
</div><div class="col-6 p-1 magmil-area">
	<a  href="<@wp.url page=''delibere_in_evidenza_home''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/ea1f964d47d790e156dafad648e21df3_d0.png" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Delibere in evidenza</h5>
      <span style="font-size: 0.85rem;">Selezione specifica dell''Archivio del CMM</span>
  	</div>
  </div>
  </a>
</div><div class="col-6 p-1 magmil-area">
	<a   href="<@wp.url page=''attivit_aistituzionale_del_cmm''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/Logo-GM-piccolo.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Giustizia Militare in Italia</h5>
      <span style="font-size: 0.85rem;">Informazioni di carattere generale</span>
  	</div>
  </div>
  </a>
</div><div class="col-6 p-1 magmil-area">
	<a   href="<@wp.url page=''attivit_aistituzionale_del_cmm''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/Palazzo_Cesi_Facciata_1.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Consiglio Magistratura Militare</h5>
      <span style="font-size: 0.85rem;">Organo di autogoverno della Magistratura Militare</span>
  	</div>
  </div>
  </a>
</div><div class="col-6 p-1 magmil-area">
	<a   href="<@wp.url page=''trasparenza''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/martello.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;"> Uffici Giudiziari Militari</h5>
      <span style="font-size: 0.85rem;">Caratteristiche ed attivit� degli Uffici Giudiziari Militari sul territorio</span>
  	</div>
  </div>
  </a>
</div>
<div class="col-6 p-1 magmil-area">
	<a   href="<@wp.url page=''trasparenza''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/trasparenza.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Trasparenza</h5>
      <span style="font-size: 0.85rem;">Informazioni e dati ai sensi del DLgs n. 33 del 14/03/2013</span>
  	</div>
  </div>
  </a>
</div><div class="col-6 p-1 magmil-area">
	<a   href="<@wp.url page=''sistemi_applicazioni''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/portatile_1.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Sistemi informativi e Applicazioni</h5>
      <span style="font-size: 0.85rem;">Sistemi Informativi e Procedure Informatiche della Giustizia Militare</span>
  	</div>
  </div>
  </a>
</div><div class="col-6 p-1 magmil-area">
	<a href="<@wp.url page=''sistemi_applicazioni''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/Banca_dati_6.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Documentazione</h5>
      <span style="font-size: 0.85rem;">Documenti, Sentenze e materiale informativo</span>
  	</div>
  </div>
  </a>
</div><div class="col-6 p-1 magmil-area">
	<a href="<@wp.url page=''incontri_di_studio''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/incontro_di studio.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Incontri di Studio</h5>
      <span style="font-size: 0.85rem;">Organizzati dal Consiglio della Magistratura Militare</span>
  	</div>
  </div>
  </a>
</div><div class="col-6 p-1 magmil-area">
	<a  href="<@wp.url page=''concorsi_e_nomine''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/siti_esterni.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;"> Link utili per la Giustizia Militare</h5>
      <span style="font-size: 0.85rem;">Siti, Portali e Risorse Internet di interesse specifico</span>
  	</div>
  </div>
  </a>
</div><div class="col-6 p-1 magmil-area">
	<a  href="<@wp.url page=''concorsi_e_nomine''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/Concorsi.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Concorsi e Nomine</h5>
      <span style="font-size: 0.85rem;">Bandi e collaborazioni nella Giustizia Militare</span>
  	</div>
  </div>
  </a>
</div><div class="col-6 p-1 magmil-area">
	<a href="<@wp.url page=''eventi_storici''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/clessidra1.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Eventi Storici</h5>
      <span style="font-size: 0.85rem;">Eventi/Notizie di utilit� per gli Utenti del Portale-GM</span>
  	</div>
  </div>
  </a>
</div>
<div class="col-6 p-1 magmil-area">
	<a href="<@wp.url page=''eventi_storici''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/Rassegna_GM.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Rassegna della Giustizia Militare</h5>
      <span style="font-size: 0.85rem;">A cura della Procura Generale Militare presso la Corte di Cassazione</span>
  	</div>
  </div>
  </a>
</div>
<div class="col-6 p-1 magmil-area">
	<a href="<@wp.url page=''eventi_storici''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/bilancia italia.jpg" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;">Processi per crimini di guerra</h5>
      <span style="font-size: 0.85rem;">Atti e Documenti</span>
  	</div>
  </div>
  </a>
</div>
<div class="col-6 p-1 magmil-area">
	<a href="<@wp.url page=''eventi_storici''/>">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-3">
  		<img src="/SIGMIL/resources/cms/images/rubrica_blu.gif" style="width: 80px; height: 80px;">
  	</div>
  	<div class="col-9">
  		<h5 class="text-primary" style="font-size: 1rem;"> Rubrica della Giustizia Militare</h5>
      <span style="font-size: 0.85rem;">Elenco e recapiti del Personale dipendente</span>
  	</div>
  </div>
  </a>
</div>
<div class="col-6 p-1 magmil-area" style="margin-left: 0px">
	<div class="m-0 row align-items-center p-2 bg-light" style="height: 130px; background-color: #E8E8E8 !important">
  	<div class="col-12">
     <a href="http://80.211.8.20:8080/SIGMIL/it/magistraturamilitareinprogress.page" class="text-primary" style="font-size: 0.85rem; margin-top: 0px">Eventi storici</a><br>
     <a  href="http://80.211.8.20:8080/SIGMIL/it/magistraturamilitareinprogress.page" class="text-primary" style="font-size: 0.85rem;">Processi per crimini di guerra</a><br>
      <a href="http://80.211.8.20:8080/SIGMIL/it/magistraturamilitareinprogress.page" class="text-primary" style="font-size: 0.85rem;">Rassegna della Giustizia Militare</a><br>
      <a href="http://80.211.8.20:8080/SIGMIL/it/magistraturamilitareinprogress.page" class="text-primary" style="font-size: 0.85rem;">Biblioteca CESI</a><br>
      <a href="https://www.csm.it//" class="text-primary" style="font-size: 0.85rem;">C.S.M. - Consiglio Superiore della Magistratura</a>
  	</div>
  </div>
</div>
</div>         </div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('htmllib','htmllib',NULL,'<#assign wp=JspTaglibs["/aps-core"]>

		                        <@wp.fragment code="jacms_content_viewer_list" escapeXml=false/>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('internal_servlet_generic_error',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.i18n key="GENERIC_ERROR" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('internal_servlet_user_not_allowed',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.i18n key="USER_NOT_ALLOWED" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer','content_viewer','jacms',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@jacms.contentInfo param="authToEdit" var="canEditThis" />
<@jacms.contentInfo param="contentId" var="myContentId" />
<#if (canEditThis?? && canEditThis)>
	<div class="bar-content-edit">
		<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/jacms/Content/edit.action?contentId=<@jacms.contentInfo param="contentId" />" class="btn btn-info">
		<@wp.i18n key="EDIT_THIS_CONTENT" /> <i class="icon-edit icon-white"></i></a>
	</div>
</#if>
<@jacms.content publishExtraTitle=true />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list','content_viewer_list','jacms',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@wp.headInfo type="JS_EXT" info="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js" />
<@jacms.contentList listName="contentList" titleVar="titleVar"
	pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" userFilterOptionsVar="userFilterOptionsVar" />
<#if (titleVar??)>
	<h1>${titleVar}</h1>
</#if>
<@wp.freemarkerTemplateParameter var="userFilterOptionsVar" valueName="userFilterOptionsVar" removeOnEndTag=true >
<@wp.fragment code="jacms_content_viewer_list_userfilters" escapeXml=false />
</@wp.freemarkerTemplateParameter>
<#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>
	<@wp.pager listName="contentList" objectName="groupContent" pagerIdFromFrame=true advanced=true offset=5>
		<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
		<@wp.fragment code="default_pagerBlock" escapeXml=false />
<#list contentList as contentId>
<#if (contentId_index >= groupContent.begin) && (contentId_index <= groupContent.end)>
	<@jacms.content contentId="${contentId}" />
</#if>
</#list>
		<@wp.fragment code="default_pagerBlock" escapeXml=false />
		</@wp.freemarkerTemplateParameter>
	</@wp.pager>
<#else>
		<p class="alert alert-info"><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
</#if>
<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
	<p class="text-right"><a class="btn btn-primary" href="<@wp.url page="${pageLinkVar}"/>">${pageLinkDescriptionVar}</a></p>
</#if>
<#assign contentList="">',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilters',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#if (userFilterOptionsVar??) && userFilterOptionsVar?has_content && (userFilterOptionsVar?size > 0)>
<div class="row-fluid"><div class="span12 padding-medium-top">
<#assign hasUserFilterError = false >
<#list userFilterOptionsVar as userFilterOptionVar>
<#if (userFilterOptionVar.formFieldErrors??) && userFilterOptionVar.formFieldErrors?has_content && (userFilterOptionVar.formFieldErrors?size > 0)>
<#assign hasUserFilterError = true >
</#if>
</#list>
<#if (hasUserFilterError)>
<div class="alert alert-error">
	<a class="close" data-dismiss="alert" href="#"><i class="icon-remove"></i></a>
	<h2 class="alert-heading"><@wp.i18n key="ERRORS" /></h2>
	<ul>
		<#list userFilterOptionsVar as userFilterOptionVar>
			<#if (userFilterOptionVar.formFieldErrors??) && (userFilterOptionVar.formFieldErrors?size > 0)>
			<#assign formFieldErrorsVar = userFilterOptionVar.formFieldErrors >
			<#list formFieldErrorsVar?keys as formFieldErrorKey>
			<li>
			<@wp.i18n key="jacms_LIST_VIEWER_FIELD" />&#32;<em>${formFieldErrorsVar[formFieldErrorKey].attributeName}</em><#if (formFieldErrorsVar[formFieldErrorKey].rangeFieldType??)>:&#32;<em><@wp.i18n key="${formFieldErrorsVar[formFieldErrorKey].rangeFieldType}" /></em></#if>&#32;<@wp.i18n key="${formFieldErrorsVar[formFieldErrorKey].errorKey}" />
			</li>
			</#list>
			</#if>
		</#list>
	</ul>
</div>
</#if>
<#assign hasUserFilterError = false >
<p><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#content-viewer-list-filters"><@wp.i18n key="SEARCH_FILTERS_BUTTON" /> <i class="icon-zoom-in icon-white"></i></button></p>
<form action="<@wp.url />" method="post" class="form-horizontal collapse" id="content-viewer-list-filters">
	<#list userFilterOptionsVar as userFilterOptionVar>
		<@wp.freemarkerTemplateParameter var="userFilterOptionVar" valueName="userFilterOptionVar" removeOnEndTag=true >
		<#if !userFilterOptionVar.attributeFilter && (userFilterOptionVar.key == "fulltext" || userFilterOptionVar.key == "category")>
			<@wp.fragment code="jacms_content_viewer_list_userfilter_met_${userFilterOptionVar.key}" escapeXml=false />
		</#if>
		<#if userFilterOptionVar.attributeFilter >
			<#if userFilterOptionVar.attribute.type == "Monotext" || userFilterOptionVar.attribute.type == "Text" || userFilterOptionVar.attribute.type == "Longtext" || userFilterOptionVar.attribute.type == "Hypertext">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Text" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "Enumerator" >
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Enumer" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "EnumeratorMap" >
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_EnumerMap" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "Number">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Number" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "Date">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Date" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "Boolean">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Boolean" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "CheckBox">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_CheckBox" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "ThreeState">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_ThreeSt" escapeXml=false />
			</#if>
		</#if>
		</@wp.freemarkerTemplateParameter>
	</#list>
	<p class="form-actions">
		<input type="submit" value="<@wp.i18n key="SEARCH" />" class="btn btn-primary" />
	</p>
</form>
</div></div>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Boolean',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<fieldset>
<legend><@wp.i18n key="${i18n_Attribute_Key}" /></legend>
<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Bool_io" escapeXml=false />
<div class="control-group">
	<div class="controls">
		<label for="${formFieldNameVar}" class="radio">
		<input name="${formFieldNameVar}" id="true_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "true")>checked="checked"</#if> value="true" type="radio" />
		<@wp.i18n key="YES"/></label>
	</div>
	<div class="controls">
		<label for="false_${formFieldNameVar}" class="radio">
		<input name="${formFieldNameVar}" id="false_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "false")>checked="checked"</#if> value="false" type="radio" />
		<@wp.i18n key="NO"/></label>
	</div>
</div>
</fieldset>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Bool_io',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameControlVar = userFilterOptionVar.formFieldNames[2] >
<input name="${formFieldNameControlVar}" type="hidden" value="true" />
<#assign formFieldNameIgnoreVar = userFilterOptionVar.formFieldNames[1] >
<#assign formFieldIgnoreValue = userFilterOptionVar.getFormFieldValue(formFieldNameIgnoreVar) >
<#assign formFieldControlValue = userFilterOptionVar.getFormFieldValue(formFieldNameControlVar) >
<div class="controls">
	<label for="ignore_${formFieldNameIgnoreVar}" class="checkbox">
	<input id="ignore_${formFieldNameIgnoreVar}" name="${formFieldNameIgnoreVar}" <#if (formFieldIgnoreValue?? && formFieldIgnoreValue == "true")>checked="checked"</#if> value="true" type="checkbox" />
	<@wp.i18n key="IGNORE" /></label>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_CheckBox',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<fieldset>
<legend><@wp.i18n key="${i18n_Attribute_Key}" /></legend>
<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Bool_io" escapeXml=false />
<div class="control-group">
	<div class="controls">
		<label for="true_${formFieldNameVar}" class="checkbox">
		<input name="${formFieldNameVar}" id="true_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "true")>checked="checked"</#if> value="true" type="checkbox" />
		<@wp.i18n key="YES"/></label>
	</div>
</div>
</fieldset>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Date',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<#assign currentLangVar ><@wp.info key="currentLang" /></#assign>

<#assign js_for_datepicker="jQuery(function($){
	$.datepicker.regional[''it''] = {
		closeText: ''Chiudi'',
		prevText: ''&#x3c;Prec'',
		nextText: ''Succ&#x3e;'',
		currentText: ''Oggi'',
		monthNames: [''Gennaio'',''Febbraio'',''Marzo'',''Aprile'',''Maggio'',''Giugno'',
			''Luglio'',''Agosto'',''Settembre'',''Ottobre'',''Novembre'',''Dicembre''],
		monthNamesShort: [''Gen'',''Feb'',''Mar'',''Apr'',''Mag'',''Giu'',
			''Lug'',''Ago'',''Set'',''Ott'',''Nov'',''Dic''],
		dayNames: [''Domenica'',''Luned&#236'',''Marted&#236'',''Mercoled&#236'',''Gioved&#236'',''Venerd&#236'',''Sabato''],
		dayNamesShort: [''Dom'',''Lun'',''Mar'',''Mer'',''Gio'',''Ven'',''Sab''],
		dayNamesMin: [''Do'',''Lu'',''Ma'',''Me'',''Gi'',''Ve'',''Sa''],
		weekHeader: ''Sm'',
		dateFormat: ''yy-mm-dd'',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''''};
});

jQuery(function($){
	if (Modernizr.touch && Modernizr.inputtypes.date) {
		$.each(	$(''input[data-isdate=true]''), function(index, item) {
			item.type = ''date'';
		});
	} else {
		$.datepicker.setDefaults( $.datepicker.regional[''${currentLangVar}''] );
		$(''input[data-isdate=true]'').datepicker({
      			changeMonth: true,
      			changeYear: true,
      			dateFormat: ''yyyy-mm-dd''
    		});
	}
});" >

<@wp.headInfo type="JS" info="entando-misc-html5-essentials/modernizr-2.5.3-full.js" />
<@wp.headInfo type="JS_EXT" info="http://code.jquery.com/ui/1.10.0/jquery-ui.min.js" />
<@wp.headInfo type="CSS_EXT" info="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.min.css" />
<@wp.headInfo type="JS_RAW" info="${js_for_datepicker}" />
<fieldset>
<legend>
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<@wp.i18n key="${i18n_Attribute_Key}" />
</legend>
<div class="control-group">
	<#assign formFieldStartNameVar = userFilterOptionVar.formFieldNames[0] >
	<#assign formFieldStartValueVar = userFilterOptionVar.getFormFieldValue(formFieldStartNameVar) >
	<label for="${formFieldStartNameVar}" class="control-label">
		<@wp.i18n key="DATE_FROM" />
	</label>
	<div class="controls">
		<input id="${formFieldStartNameVar}" name="${formFieldStartNameVar}" value="${formFieldStartValueVar?default("")}" type="text" data-isdate="true" class="input-xlarge" />
	</div>
</div>
<div class="control-group">
	<#assign formFieldEndNameVar = userFilterOptionVar.formFieldNames[1] >
	<#assign formFieldEndValueVar = userFilterOptionVar.getFormFieldValue(formFieldEndNameVar) >
	<label for="${formFieldEndNameVar}" class="control-label">
		<@wp.i18n key="DATE_TO" />
	</label>
	<div class="controls">
		<input id="${formFieldEndNameVar}" name="${formFieldEndNameVar}" value="${formFieldEndValueVar?default("")}" type="text" data-isdate="true" class="input-xlarge" />
	</div>
</div>
</fieldset>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Enumer',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<div class="control-group">
	<label for="${formFieldNameVar}" class="control-label"><@wp.i18n key="${i18n_Attribute_Key}" /></label>
	<div class="controls">
		<select name="${formFieldNameVar}" id="${formFieldNameVar}" class="input-xlarge">
			<option value=""><@wp.i18n key="ALL" /></option>
			<#list userFilterOptionVar.attribute.items as enumeratorItemVar>
			<option value="${enumeratorItemVar}" <#if (formFieldValue??) && (enumeratorItemVar == formFieldValue)>selected="selected"</#if> >${enumeratorItemVar}</option>
			</#list>
		</select>
	</div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_EnumerMap',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<div class="control-group">
	<label for="${formFieldNameVar}" class="control-label"><@wp.i18n key="${i18n_Attribute_Key}" /></label>
	<div class="controls">
		<select name="${formFieldNameVar}" id="${formFieldNameVar}" class="input-xlarge">
			<option value=""><@wp.i18n key="ALL" /></option>
			<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
			<option value="${enumeratorMapItemVar.key}" <#if (formFieldValue??) && (enumeratorMapItemVar.key == formFieldValue)>selected="selected"</#if> >${enumeratorMapItemVar.value}</option>
			</#list>
		</select>
	</div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Number',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<fieldset>
<legend>
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<@wp.i18n key="${i18n_Attribute_Key}" />
</legend>
<div class="control-group">
	<#assign formFieldStartNameVar = userFilterOptionVar.formFieldNames[0] >
	<#assign formFieldStartValueVar = userFilterOptionVar.getFormFieldValue(formFieldStartNameVar) >
	<label for="${formFieldStartNameVar}" class="control-label">
		<@wp.i18n key="NUMBER_FROM" />
	</label>
	<div class="controls">
		<input id="${formFieldStartNameVar}" name="${formFieldStartNameVar}" value="${formFieldStartValueVar?default("")}" type="number" class="input-medium" />
	</div>
</div>
<div class="control-group">
	<#assign formFieldEndNameVar = userFilterOptionVar.formFieldNames[1] >
	<#assign formFieldEndValueVar = userFilterOptionVar.getFormFieldValue(formFieldEndNameVar) >
	<label for="${formFieldEndNameVar}" class="control-label">
		<@wp.i18n key="NUMBER_TO" />
	</label>
	<div class="controls">
		<input id="${formFieldEndNameVar}" name="${formFieldEndNameVar}" value="${formFieldEndValueVar?default("")}" type="number" class="input-medium" />
	</div>
</div>
</fieldset>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Text',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<div class="control-group">
	<label for="${formFieldNameVar}" class="control-label"><@wp.i18n key="${i18n_Attribute_Key}" /></label>
	<div class="controls">
		<input name="${formFieldNameVar}" id="${formFieldNameVar}" value="${formFieldValue}" type="text" class="input-xlarge"/>
	</div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_ThreeSt',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<fieldset>
<legend><@wp.i18n key="${i18n_Attribute_Key}" /></legend>
<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Bool_io" escapeXml=false />
<div class="control-group">
	<div class="controls">
		<label for="true_${formFieldNameVar}" class="radio">
		<input name="${formFieldNameVar}" id="true_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "true")>checked="checked"</#if> value="true" type="radio" />
		<@wp.i18n key="YES"/></label>
		<label for="false_${formFieldNameVar}" class="radio">
		<input name="${formFieldNameVar}" id="false_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "false")>checked="checked"</#if> value="false" type="radio" />
		<@wp.i18n key="NO"/></label>
		<label for="both_${formFieldNameVar}" class="radio">
		<input name="${formFieldNameVar}" id="both_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "both")>checked="checked"</#if> value="both" type="radio" />
		<@wp.i18n key="BOTH"/></label>
	</div>
</div>
</fieldset>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_met_category',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign userFilterCategoryCodeVar = userFilterOptionVar.userFilterCategoryCode?default("") >
<@wp.categories var="systemCategories" titleStyle="prettyFull" root="${userFilterCategoryCodeVar}" />
<div class="control-group">
	<label for="category" class="control-label"><@wp.i18n key="CATEGORY" /></label>
	<div class="controls">
		<select id="category" name="${formFieldNameVar}" class="input-xlarge">
			<option value=""><@wp.i18n key="ALL" /></option>
			<#list systemCategories as systemCategory>
			<option value="${systemCategory.key}" <#if (formFieldValue == systemCategory.key)>selected="selected"</#if> >${systemCategory.value}</option>
			</#list>
		</select>
	</div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_met_fulltext',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<div class="control-group">
    <label for="${formFieldNameVar}" class="control-label"><@wp.i18n key="TEXT" /></label>
    <div class="controls">
        <input name="${formFieldNameVar}" id="${formFieldNameVar}" value="${formFieldValue}" type="text" class="input-xlarge"/>
    </div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_row_content_viewer_list','row_content_viewer_list','jacms',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar"
	pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />
<#if (titleVar??)>
	<h1>${titleVar}</h1>
</#if>
<#if (contentInfoList??) && (contentInfoList?has_content) && (contentInfoList?size > 0)>
	<@wp.pager listName="contentInfoList" objectName="groupContent" pagerIdFromFrame=true advanced=true offset=5>
	<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
	<@wp.fragment code="default_pagerBlock" escapeXml=false />
	<#list contentInfoList as contentInfoVar>
	<#if (contentInfoVar_index >= groupContent.begin) && (contentInfoVar_index <= groupContent.end)>
		<#if (contentInfoVar[''modelId'']??)>
		<@jacms.content contentId="${contentInfoVar[''contentId'']}" modelId="${contentInfoVar[''modelId'']}" />
		<#else>
		<@jacms.content contentId="${contentInfoVar[''contentId'']}" />
		</#if>
	</#if>
	</#list>
	<@wp.fragment code="default_pagerBlock" escapeXml=false />
	</@wp.freemarkerTemplateParameter>
	</@wp.pager>
</#if>
<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
	<p class="text-right"><a class="btn btn-primary" href="<@wp.url page="${pageLinkVar}"/>">${pageLinkDescriptionVar}</a></p>
</#if>
<#assign contentInfoList="">',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('login_form','login_form',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<h1><@wp.i18n key="RESERVED_AREA" /></h1>
<#if (Session.currentUser.username != "guest") >
	<p><@wp.i18n key="WELCOME" />, <em>${Session.currentUser}</em>!</p>
	<#if (Session.currentUser.entandoUser) >
	<table class="table table-condensed">
		<tr>
			<th><@wp.i18n key="USER_DATE_CREATION" /></th>
			<th><@wp.i18n key="USER_DATE_ACCESS_LAST" /></th>
			<th><@wp.i18n key="USER_DATE_PASSWORD_CHANGE_LAST" /></th>
		</tr>
		<tr>
			<td>${Session.currentUser.creationDate?default("-")}</td>
			<td>${Session.currentUser.lastAccess?default("-")}</td>
			<td>${Session.currentUser.lastPasswordChange?default("-")}</td>
		</tr>
	</table>
		<#if (!Session.currentUser.credentialsNotExpired) >
		<div class="alert alert-block">
			<p><@wp.i18n key="USER_STATUS_EXPIRED_PASSWORD" />: <a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/editPassword.action"><@wp.i18n key="USER_STATUS_EXPIRED_PASSWORD_CHANGE" /></a></p>
		</div>
		</#if>
	</#if>
	<@wp.ifauthorized permission="enterBackend">
	<div class="btn-group">
		<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/main.action?request_locale=<@wp.info key="currentLang" />" class="btn"><@wp.i18n key="ADMINISTRATION" /></a>
	</div>
	</@wp.ifauthorized>
	<p class="pull-right"><a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action" class="btn"><@wp.i18n key="LOGOUT" /></a></p>
	<@wp.pageWithWidget widgetTypeCode="userprofile_editCurrentUser" var="userprofileEditingPageVar" listResult=false />
	<#if (userprofileEditingPageVar??) >
	<p><a href="<@wp.url page="${userprofileEditingPageVar.code}" />" ><@wp.i18n key="userprofile_CONFIGURATION" /></a></p>
	</#if>
<#else>
	<#if (accountExpired?? && accountExpired == true) >
	<div class="alert alert-block alert-error">
		<p><@wp.i18n key="USER_STATUS_EXPIRED" /></p>
	</div>
	</#if>
	<#if (wrongAccountCredential?? && wrongAccountCredential == true) >
	<div class="alert alert-block alert-error">
		<p><@wp.i18n key="USER_STATUS_CREDENTIALS_INVALID" /></p>
	</div>
	</#if>
	<form action="<@wp.url/>" method="post" class="form-horizontal margin-medium-top">
		<#if (RequestParameters.returnUrl??) >
		<input type="hidden" name="returnUrl" value="${RequestParameters.returnUrl}" />
		</#if>
		<div class="control-group">
			<label for="username" class="control-label"><@wp.i18n key="USERNAME" /></label>
			<div class="controls">
				<input id="username" type="text" name="username" class="input-xlarge" />
			</div>
		</div>
		<div class="control-group">
			<label for="password" class="control-label"><@wp.i18n key="PASSWORD" /></label>
			<div class="controls">
				<input id="password" type="password" name="password" class="input-xlarge" />
			</div>
		</div>
		<div class="form-actions">
			<input type="submit" value="<@wp.i18n key="SIGNIN" />" class="btn btn-primary" />
		</div>
	</form>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('menu_rapido','menu_rapido',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.currentPage param="code" var="currentPageCode" />
<#assign firstLevelCounter = 0/>
<#assign secondLevelCounter = 1/>
<#assign thirdLevelCounter = 2/>
<#assign fourthLevelCounter = 3/>
<#assign fifthLevelCounter = 4/>
<#assign sixthLevelCounter = 5/>

<#assign pagesWithTargetBlank = ["sottosegretari_di_stato","il_capo_di_gabinetto"] />

<style>
#root-menu{
    border-top: 1px solid darkgrey;
}
#root-menu .card-header{
    padding: 0;
    background-color: white;
    border-color: darkgrey
}

#root-menu .card{
    border: none
}

#root-menu .row{
    margin: 0
}

.collapsing-arrow:hover{
    text-decoration: none !important
}

.third-level-menu{
    background-color: #eee;
}

.fourth-level-menu{
    background-color: #e6e6e6
}
</style>

<div class="bg-primary d-sm-none d-flex justify-content-between align-items-center text-white p-2">
   <span>Menu</span>
   <i class="fa fa-bars" data-toggle="collapse" data-target="#root-menu"></i>
</div>
<div id="root-menu" class="collapse d-sm-block">
        <div class="card">
                <div class="card-header">
                   <div class="row first-level-menu">
                      <h6 class="col-10 mb-0 p-2">
                         <a href="<@wp.url page=''homepage''/>" style="text-decoration: none"><i class="fa fa-home"></i> Home</a>
                      </h6>
                  </div>
                </div>
             </div>
    <@wp.nav spec="code(homepage).subtree(1)" var="firstLevelPage">
   <#if firstLevelCounter != 0>
   <@wp.currentPage param="childOf" targetPage="${currentPageCode}" var="isThisBranchFirstLevel" />
   <@wp.pageInfo info="hasChild" pageCode="${currentPageCode}"   var="firstLevelHasChild" />
   <#if isThisBranchFirstLevel == ''true''>
   <div class="card">
      <div class="card-header">
         <div class="row first-level-menu">
            <h6 class="col-10 mb-0 p-2">
               <a <#if pagesWithTargetBlank?seq_contains(firstLevelPage.code)> target="_blank" </#if> href="${firstLevelPage.url}" <#if firstLevelPage.code == currentPageCode>class="font-weight-bold show"</#if>>${firstLevelPage.title}</a>
            </h6>
            <a class="col-2 collapsing-arrow d-flex justify-content-center align-items-center p-2 <#if firstLevelHasChild == ''false''>d-none</#if> " data-toggle="collapse" href="#collapse${firstLevelCounter}"><i class="fa fa-chevron-down"></i></a>
        </div>
      </div>
   </div>
   <div id="collapse${firstLevelCounter}" class=" collapse <#if isThisBranchFirstLevel == ''true''>  show </#if> " data-parent="#root-menu">
      <div class="card-body p-0">
         <div id="accordion${firstLevelCounter}">
            <@wp.nav spec="code(${firstLevelPage.code}).subtree(1)" var="secondLevelPage">
            <#if secondLevelCounter != 1>
            <@wp.currentPage param="childOf" targetPage="${secondLevelPage.code}" var="isThisBranchSecondLevel" />
   <@wp.pageInfo info="hasChild" pageCode="${secondLevelPage.code}"   var="secondLevelHasChild" />
            <div class="card">
               <div class="card-header">
                  <div class="row third-level-menu">
                     <h6 class="mb-0 col-10 align-self-center p-2 pl-3">
                        <a <#if pagesWithTargetBlank?seq_contains(secondLevelPage.code)> target="_blank" </#if> class="<#if secondLevelPage.code == currentPageCode>font-weight-bold show</#if>" href="${secondLevelPage.url}" >${secondLevelPage.title}</a>
                     </h6>
                     <a class="col-2  collapsing-arrow d-flex justify-content-center align-items-center p-2  <#if secondLevelHasChild == ''false''>d-none</#if>" data-toggle="collapse" href="#collapse${firstLevelCounter}${secondLevelCounter}"  ><i class="fa fa-chevron-down"></i></a>
                  </div>
               </div>
               <div id="collapse${firstLevelCounter}${secondLevelCounter}" class="collapse <#if isThisBranchSecondLevel == ''true''>show</#if>" data-parent="#accordion${firstLevelCounter}">
                  <div class="card-body p-0">
                     <@wp.nav spec="code(${secondLevelPage.code}).subtree(1)" var="thirdLevelPage">
                     <#if thirdLevelCounter != 2>
                     <@wp.currentPage param="childOf" targetPage="${thirdLevelPage.code}" var="isThisBranchThirdLevel" />
 <@wp.pageInfo info="hasChild" pageCode="${thirdLevelPage.code}"   var="thirdLevelHasChild" />
                     <div class="card">
                        <div class="card-header">
                           <div class="row third-level-menu">
                              <h6 class="mb-0 col-10 p-2 pl-4">
                                 <a <#if pagesWithTargetBlank?seq_contains(thirdLevelPage.code)> target="_blank" </#if> class="<#if thirdLevelPage.code == currentPageCode>font-weight-bold show</#if>" href="${thirdLevelPage.url}">${thirdLevelPage.title}</a>
                              </h6>
                              <a class="col-2 collapsing-arrow d-flex justify-content-center align-items-center p-2 <#if thirdLevelHasChild== ''false''>d-none</#if>" data-toggle="collapse" href="#collapse${firstLevelCounter}${secondLevelCounter}${thirdLevelCounter}"  ><i class="fa fa-chevron-down"></i></a>
                           </div>
                        </div>
                     </div>
                     <div id="collapse${firstLevelCounter}${secondLevelCounter}${thirdLevelCounter}" class="collapse <#if isThisBranchThirdLevel == ''true''>show</#if>" data-parent="#accordion${thirdLevelCounter}">
                        <div class="card-body p-0">
                           <@wp.nav spec="code(${thirdLevelPage.code}).subtree(1)" var="fourthLevelPage">
                           <#if fourthLevelCounter != 3>
                           <div class="card">
                              <div class="card-header">
                                 <div class="row fourth-level-menu">
                                    <h6 class="mb-0 col-11 p-2 pl-5">
                                       <a <#if pagesWithTargetBlank?seq_contains(fourthLevelPage.code)> target="_blank" </#if> class="<#if fourthLevelPage.code == currentPageCode>font-weight-bold</#if>" href="${fourthLevelPage.url}" >${fourthLevelPage.title}</a>
                                    </h6>                                  
                                 </div>
                              </div>
                           </div>
                              </#if>
                              <#assign fourthLevelCounter++ />
                              </@wp.nav>
                        </div>
                     </div>
                        </#if>
                        <#assign thirdLevelCounter++ />
                        <#assign fourthLevelCounter=3 />
                        </@wp.nav>
                     </div>
                  </div>
               </div>
               </#if>
               <#assign secondLevelCounter++ />
               <#assign thirdLevelCounter=2 />
               </@wp.nav>
            </div>
         </div>
      </div>
      </#if>
      </#if>
      <#assign firstLevelCounter++ />
      <#assign secondLevelCounter =1 />
      </@wp.nav>
   </div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('menu_test','menu_test',NULL,'<%@ taglib prefix="wp" uri="/aps-core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<wp:currentPage param="code" var="currentPageCode" />
<c:set var="currentPageCode" value="${currentPageCode}" />
<c:set var="previousPage" value="${null}" />

<div class="well well-small">

<ul class="nav nav-list">
<wp:nav var="page">
<c:if test="${previousPage.code != null}">
	<c:set var="previousLevel" value="${previousPage.level}" />
	<c:set var="level" value="${page.level}" />
	<%@ include file="entando-widget-navigation_menu_include.jsp" %>
</c:if>

	<c:set var="previousPage" value="${page}" />
</wp:nav>
<c:if test="${previousPage != null}">
	<c:set var="previousLevel" value="${previousPage.level}" />
	<c:set var="level" value="${0}"  scope="request" /> <%-- we are out, level is 0 --%>
	<%@ include file="entando-widget-navigation_menu_include.jsp" %>
	<c:if test="${previousLevel != 0}">
		<c:forEach begin="${0}" end="${previousLevel -1}"></ul></li></c:forEach>
	</c:if>
</c:if>
</ul>

</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('messages_system','messages_system',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<#assign currentPageCode><@wp.currentPage param="code" /></#assign>

<#if (currentPageCode == ''notfound'')>
<div class="alert alert-error alert-block">
	<h1 class="alert-heading"><@wp.i18n key="PAGE_NOT_FOUND" escapeXml=false /></h1>
</div>
</#if>
<#if (currentPageCode == ''errorpage'')>
<div class="alert alert-error alert-block">
	<h1 class="alert-heading"><@wp.i18n key="GENERIC_ERROR" escapeXml=false /></h1>
</div>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('normativa','normativa',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Normativa</i></b></h4>
<hr>
<span style="height:45px; display:block"><br></span><div class="contenutoPagine">
                <div><font face="Arial"><font color="#006400" size="2">
<p align="justify"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt"><strong>Ufficiali Giudici militari</strong></span></p>
<p align="justify"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt"></span><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt"><font color="#000000">Gli articoli 54 e 57 del Decreto Legislativo 15 marzo 2010, n. 66 � Codice dell�ordinamento militare � stabiliscono che il Tribunale militare e la Corte Militare di Appello giudicano, altres�, con l�intervento, rispettivamente, (..)<i style="mso-bidi-font-style: normal"> di un militare dell''Esercito italiano, della Marina militare, dell''Aeronautica militare, dell'' Arma dei Carabinieri o della Guardia di finanza di grado pari a quello dell''imputato e comunque non inferiore al grado di ufficiale, estratto a sorte, con funzioni di giudice. (�)</i>, e (�) <i style="mso-bidi-font-style: normal">di due militari dell''Esercito italiano, della Marina militare, dell''Aeronautica militare, dell''Arma dei Carabinieri o della Guardia di finanza, di grado pari a quello dell''imputato e, comunque, non inferiore a tenente colonnello, estratti a sorte, con funzioni di giudice.<!--?xml:namespace prefix = o ns = "urn:schemas-microsoft-com:office:office" /--><o:p></o:p></i></font></span></span></p>
<p style="TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt" class="MsoNormal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt"><font color="#000000">L�art. 54 citato, comma <b style="mso-bidi-font-weight: normal">3</b>, stabilisce che, <i style="mso-bidi-font-style: normal">l''estrazione a sorte dei giudici di cui al comma 2, lettera c), si effettua tra gli ufficiali, aventi il grado richiesto, che prestano servizio nella circoscrizione del Tribunale militare</i>. I successivi commi disciplinano le modalit� e il luogo dell�estrazione <i style="mso-bidi-font-style: normal">(Le estrazioni a sorte, previo avviso affisso in apposito albo, sono effettuate, nell''aula di udienza aperta al pubblico, dal presidente, alla presenza del pubblico ministero, con l''assistenza di un ausiliario, che redige verbale.);</i> la durata dell�esercizio delle funzioni <i style="mso-bidi-font-style: normal">(I giudici estratti a sorte durano in funzione due mesi e proseguono nell''esercizio delle funzioni sino alla conclusione dei dibattimenti in corso.)</i>; la cadenza dell�estrazione <i style="mso-bidi-font-style: normal">(L''estrazione a sorte avviene ogni sei mesi, distintamente per ognuno dei bimestri successivi. Sono estratti, per ogni giudice, due supplenti.)<o:p></o:p></i></font></span></p>
<p style="TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt" class="MsoNormal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt"><font color="#000000">La specifica disciplina � infine completata dagli articoli 288 e 289 del codice penale militare di pace. <o:p></o:p></font></span></p>
<p style="TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt" class="MsoNormal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt"><font color="#000000">La prima disposizione (art. 288) si limita ad estendere ai giudici militari le disposizioni del codice di procedura penale comune in ordine agli istituti della incompatibilit�, ricusazione ed astensione. La seconda (289) prevede le ipotesi particolari di incompatibilit� nei procedimenti penali militari, stabilendo che non possono far parte degli organi giudiziari militari i seguenti soggetti:<o:p></o:p></font></span></p>
<p style="TEXT-ALIGN: justify; TEXT-INDENT: -18pt; MARGIN: 0cm 0cm 6pt 36pt; mso-list: l0 level1 lfo1; mso-add-space: auto; tab-stops: 45.8pt 91.6pt 137.4pt 183.2pt 229.0pt 274.8pt 320.6pt 366.4pt 412.2pt 458.0pt 503.8pt 549.6pt 595.4pt 641.2pt 687.0pt 732.8pt" class="MsoListParagraphCxSpFirst"><font color="#000000"><i style="mso-bidi-font-style: normal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt; mso-fareast-font-family: Arial"><span style="mso-list: Ignore">1.<span style="FONT: 7pt &#39;Times New Roman&#39;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></i><i style="mso-bidi-font-style: normal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt">colui che � stato offeso dal reato; <o:p></o:p></span></i></font></p>
<p style="TEXT-ALIGN: justify; TEXT-INDENT: -18pt; MARGIN: 0cm 0cm 6pt 36pt; mso-list: l0 level1 lfo1; mso-add-space: auto; tab-stops: 45.8pt 91.6pt 137.4pt 183.2pt 229.0pt 274.8pt 320.6pt 366.4pt 412.2pt 458.0pt 503.8pt 549.6pt 595.4pt 641.2pt 687.0pt 732.8pt" class="MsoListParagraphCxSpMiddle"><font color="#000000"><i style="mso-bidi-font-style: normal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt; mso-fareast-font-family: Arial"><span style="mso-list: Ignore">2.<span style="FONT: 7pt &#39;Times New Roman&#39;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></i><i style="mso-bidi-font-style: normal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt">gli ufficiali della compagnia, o reparto corrispondente, cui<span style="mso-spacerun: yes">&nbsp; </span>appartiene l''imputato, e gli ufficiali che anno partecipato a un precedente giudizio disciplinare per lo stesso fatto, o che<span style="mso-spacerun: yes">&nbsp; </span>comunque hanno avuto una diretta ingerenza nella repressione disciplinare del fatto stesso; <o:p></o:p></span></i></font></p>
<p style="TEXT-ALIGN: justify; TEXT-INDENT: -18pt; MARGIN: 0cm 0cm 6pt 36pt; mso-list: l0 level1 lfo1; mso-add-space: auto; tab-stops: 45.8pt 91.6pt 137.4pt 183.2pt 229.0pt 274.8pt 320.6pt 366.4pt 412.2pt 458.0pt 503.8pt 549.6pt 595.4pt 641.2pt 687.0pt 732.8pt" class="MsoListParagraphCxSpMiddle"><font color="#000000"><i style="mso-bidi-font-style: normal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt; mso-fareast-font-family: Arial"><span style="mso-list: Ignore">3.<span style="FONT: 7pt &#39;Times New Roman&#39;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></i><i style="mso-bidi-font-style: normal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt">gli ufficiali che si trovavano immediatamente agli ordini dell''imputato al tempo in cui fu commesso il reato o iniziato il procedimento penale; <o:p></o:p></span></i></font></p>
<p style="TEXT-ALIGN: justify; TEXT-INDENT: -18pt; MARGIN: 0cm 0cm 6pt 36pt; mso-list: l0 level1 lfo1; mso-add-space: auto; tab-stops: 45.8pt 91.6pt 137.4pt 183.2pt 229.0pt 274.8pt 320.6pt 366.4pt 412.2pt 458.0pt 503.8pt 549.6pt 595.4pt 641.2pt 687.0pt 732.8pt" class="MsoListParagraphCxSpLast"><font color="#000000"><i style="mso-bidi-font-style: normal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt; mso-fareast-font-family: Arial"><span style="mso-list: Ignore">4.<span style="FONT: 7pt &#39;Times New Roman&#39;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></i><i style="mso-bidi-font-style: normal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt">l''ufficiale che ha proceduto ad atti preliminari all''istruzione<o:p></o:p></span></i></font></p>
<p style="TEXT-ALIGN: justify; MARGIN: 0cm 0cm 10pt" class="MsoNormal"><font color="#000000"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt">Da ultimo, la posizione dei giudici militari appartenenti alle Forze Armate sotto il profilo disciplinare � assimilata a quella dei giudici togati. In tal senso l�articolo 67, comma 2, Dlgs 66/2010 citato dispone: (�)</span><span style="FONT-FAMILY: &#39;Palatino Linotype&#39;,&#39;serif&#39;"><font size="3"> </font></span><i style="mso-bidi-font-style: normal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt">L''azione disciplinare nei confronti dei giudici militari appartenenti alle Forze armate � esercitata dal Ministro della difesa o dal procuratore generale militare presso la Corte di Cassazione. Si applicano a questi ultimi le disposizioni del comma 1 e dell''articolo 61, comma 1. disposizioni queste ultime con le quali si stabilisce che</span></i><i style="mso-bidi-font-style: normal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Palatino Linotype&#39;,&#39;serif&#39;; FONT-SIZE: 10pt"> </span></i><i style="mso-bidi-font-style: normal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt">�Il Consiglio ha, per i magistrati militari, le stesse attribuzioni previste per il Consiglio superiore della magistratura, ivi comprese quelle concernenti i procedimenti disciplinari, sostituiti al Ministro della giustizia e al procuratore generale presso la Corte di Cassazione, rispettivamente, il Ministro della difesa e il procuratore generale militare presso la Corte di Cassazione.�</span></i><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt"><o:p></o:p></span></font></p>
<p style="TEXT-ALIGN: justify; MARGIN: 0cm 0cm 10pt" class="MsoNormal"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: &#39;Arial&#39;,&#39;sans-serif&#39;; FONT-SIZE: 10pt"><font color="#000000">Il Consiglio della Magistratura Militare, oltre a varie delibere in materia, � intervenuto a disciplinare la materia relativa alle cause di totale o parziale esclusione o esonero dalle funzioni di giudice militare con la circolare n. 49 del 2003, aggiornata con una successiva delibera del 2009.<o:p></o:p></font></span></p></font></font>
 
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('progetto_sigmil','progetto_sigmil',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>ProgettoSigmil</i></b></h4>
<hr>

<div><p class="Stile1" align="center"><span class="Stile23"><font face="Arial"><font color="#6495ed"><strong><span class="Stile18"><strong><font color="#0000cd">SIGMIL</font> </strong></span><font color="#000000">- </font><span class="Stile18"><strong><font color="#0000cd">S</font></strong></span></strong><span class="Stile26"><font color="#000000">istema</font></span><strong> <span class="Stile18"><strong><font color="#0000cd">I</font></strong></span></strong><span class="Stile26"><font color="#000000">nformativo</font></span><strong> <span class="Stile18"><strong><font color="#0000cd">G</font></strong></span></strong><span class="Stile26"><font color="#000000">iustizia</font></span><strong> <span class="Stile18"><strong><font color="#0000cd">MIL</font></strong></span></strong></font><span class="Stile26"><font color="#000000">ilitare</font></span></font></span></p>
<div align="center"><font color="#6495ed" size="2" face="Arial"><img border="0" align="center" src="/SIGMIL/resources/static/img/sigmil1.gif"></font></div>
<p align="justify"><font face="Arial"><font size="2"></font></font></p>
<p align="justify"><font face="Arial"><font size="2"></font></font>&nbsp;</p>
<p align="justify"><font face="Arial"><font size="2">Il principale obiettivo del Sistema SIGMIL <i style="mso-bidi-font-style: normal">(Sistema Informativo per la Gestione del Procedimento Penale presso gli Uffici Giudiziari Militari)</i> � la gestione informatizzata del procedimento penale negli Uffici Giudiziari Militari, tenendo conto di tutti gli eventi significativi della vita del fascicolo, dalla sua nascita alla sua archiviazione. SIGMIL � quindi in grado di svolgere le seguenti macro-funzioni:</font><!--?xml:namespace prefix = o ns = "urn:schemas-microsoft-com:office:office" /--><o:p><font size="2">&nbsp;</font></o:p></font></p><font face="Arial">
<ul style="MARGIN-TOP: 0cm" type="square">
<li style="MARGIN: 0cm 0cm 0pt; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2">gestione dell''iter del procedimento penale militare, presso gli Enti: Procura Militare, Ufficio GIP/GUP del Tribunale Militare, Tribunale Militare, Corte Militare di Appello, Procura Generale Militare, Cassazione e Sorveglianza</font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2">trattamento del fascicolo elettronico del procedimento, con trasferimento dello stesso da un Ente ad un altro (passaggio per fase)</font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2">informatizzazione delle attivit� giudiziarie con controllo delle scadenze</font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2">gestione documentale dell''intero iter del Procedimento penale</font></div></li></ul>
<p style="MARGIN: 0cm 0cm 0pt" class="MsoNormal" align="justify"><font size="2">L''applicazione � centralizzata in un <u>Sistema Informativo unico</u> per tutto il territorio nazionale dove i vari Uffici accedono in maniera logicamente separata ai fascicoli di loro competenza. Viene garantita, inoltre, l''aderenza al codice di rito per quanto riguarda i momenti di segretezza e condivisione delle informazioni legate ai fascicoli trattati. </font><font size="2">Le sedi degli Uffici Giudiziari Militari, sull''intero territorio nazionale, che sono interessate dal nuovo Sistema SIGMIL sono le seguenti:</font></p>
<p style="MARGIN: 0cm 0cm 0pt" class="MsoNormal" align="justify"><o:p><font size="2">&nbsp;</font></o:p></p>
<ul style="MARGIN-TOP: 0cm" type="square">
<li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo2; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2">Procura Generale Militare presso <!--?xml:namespace prefix = st1 ns = "urn:schemas-microsoft-com:office:smarttags" /--><st1:personname w:st="on" productid="la Corte">la Corte</st1:personname> di Cassazione</font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo2; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2">Corte Militare di Appello in Roma </font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo2; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2">Procura Generale presso <st1:personname w:st="on" productid="la Corte Militare">la Corte Militare</st1:personname> di Appello in Roma</font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo2; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2">Tribunale Militare di Sorveglianza in Roma</font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo2; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2">Tribunali Militari di: Roma, Verona e Napoli <span style="FONT-FAMILY: &#39;Arial Unicode MS&#39;,&#39;sans-serif&#39;; mso-fareast-font-family: &#39;Times New Roman&#39;; mso-bidi-font-family: &#39;Times New Roman&#39;"><o:p></o:p></span></font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo2; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2">Procure Militari di: Roma, Verona e Napoli</font></div></li></ul>
</font><p style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo2; tab-stops: list 36.0pt" class="MsoNormal" align="justify"><font face="Arial"></font><font size="2"><em><strong><font size="2" face="Arial"></font></strong></em></font>&nbsp;</p>
<p style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo2; tab-stops: list 36.0pt" class="MsoNormal" align="justify"><font size="2"><em><strong><font color="#000080" size="2" face="Arial">l''ambiente tecnologico</font></strong></em></font></p>
<p style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo2; tab-stops: list 36.0pt" class="MsoNormal" align="justify"><font size="2"><strong><em><font color="#000080" face="Arial"></font></em></strong></font></p>
<div align="justify">L''architettura informatica del SIGMIL � basata sul modello standard di una <i>Web Application</i> di tipo 3-tier (3 livelli) costituito dai livelli logici seguenti:</div>
<ol>
<li>
<div align="justify"><font size="2" face="Arial">Livello "Presentazione" (Client)</font></div>
</li><li>
<div align="justify"><font size="2" face="Arial">Livello "Applicazione" (Application Server)</font></div>
</li><li>
<div align="justify"><font size="2" face="Arial">Livello "Dati" (Database Server)</font></div></li></ol>
<p style="MARGIN: 0cm 0cm 0pt" class="MsoNormal" align="justify"><font size="2" face="Arial">A livello realizzativo il modello logico 3-tier viene implementato sulla piattaforma J2EE, in grado di gestire infrastrutture tecnologiche e supportare servizi web per la realizzazione di applicazioni sicure, scalabili, distribuite ed interoperabili: pi� in dettaglio, il software applicativo viene modularizzato mediante il ricorso ai componenti <i style="mso-bidi-font-style: normal">Servlet, Enterprise Java Beans (EJB)</i> e <i style="mso-bidi-font-style: normal">Java Server Pages (JSP),</i> tecnologie in grado di supportare la struttura architetturale <i style="mso-bidi-font-style: normal">Model-View-Controller (MVC), </i>realizzando di fatto la completa separazione funzionale dei componenti dell''applicazione, a favore della riusabilit� e manutenibilit� del codice. Nella scelta dello <i style="mso-bidi-font-style: normal">stack</i> delle tecnologie che supportano la nuova applicazione, si � deciso di privilegiare per quanto possibile il riuso degli standard, delle infrastrutture e dei prodotti gi� presenti presso l''Amministrazione della Difesa. L''ambiente tecnologico del SIGMIL si basa sulle seguenti componenti fondamentali:</font></p>
<p style="MARGIN: 0cm 0cm 0pt" class="MsoNormal" align="justify"><o:p><font size="2" face="Arial">&nbsp;</font></o:p></p>
<ul style="MARGIN-TOP: 0cm" type="square">
<li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo1; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2" face="Arial">Linguaggio di sviluppo Java (J2EE)</font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo1; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2" face="Arial">Web Server e Application Server "Apache/Tomcat"</font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo1; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2" face="Arial">Framework di sviluppo STRUTS</font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo1; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2" face="Arial">RDBMS Oracle</font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo1; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><font size="2" face="Arial">Piattaforma SW di Gestione Documentale "IBM/FileNet"</font></div>
</li><li style="MARGIN: 0cm 0cm 0pt; mso-list: l1 level1 lfo1; tab-stops: list 36.0pt" class="MsoNormal">
<div align="justify"><span style="mso-ansi-language: EN-GB" lang="EN-GB"><font face="Arial"><font size="2">Web Browser standard per PC Client (MS Internet Explorer versione 6 o superiore)</font></font></span></div></li></ul>
<p class="Stile16" align="center"><font size="2" face="Arial"><img border="0"  src="/SIGMIL/resources/static/img/sigmil2.gif"></font></p>
<p class="Stile16 Stile18" align="left"><em><strong><font size="2" face="Arial"></font></strong></em>&nbsp;</p>
<p class="Stile16 Stile18" align="left"><em><strong><font color="#000080" size="2" face="Arial">La Gestione Documentale </font></strong></em></p>
<p class="Stile16" align="justify"><font size="2" face="Arial">SIGMIL implementa appieno tutte le funzionalit� di <em>Gestione Documentale </em>grazie ad una nuova e specifica componente software dedicata per tale scopo. Infatti, il crescente volume di documenti circolanti negli Uffici della Giustizia Militare, unito alle necessit� di conservazione e recupero dei dati ed alla costituzione di una base documentale informatizzata per ciascun fascicolo gestito, richiedono l''impiego di un sistema specializzato per l''archiviazione documentale. </font></p>
<p class="Stile16" align="justify"><font size="2" face="Arial">Il Sistema di archiviazione documentale fornito con SIGMIL, conforme alla direttiva CNIPA nr. 42/2001 e s.m.i., rappresenta una piattaforma estremamente personalizzabile per rispondere alle diverse esigenze dell''Amministrazione della Difesa; esso, oltre ad assolvere alla funzione di registrazione del documento ed alla conservazione dei <em>files</em> ad essi associati, rende disponibile un insieme di funzionalit� per la gestione dell''intero ciclo di vita del documento stesso. Il Sistema � caratterizzato da elevata flessibilit� di reperimento delle informazioni sia tramite interrogazioni dirette (con chiavi) che attraverso meccanismi di navigazione ipertestuale, oltre alla possibilit� di definire precisi "stati" per un documento, a ciascuno dei quali sono associate possibili operazioni (es. sola consultazione, aggiornamento, revisione, ecc.). </font></p></div>
              </div>
              <div></div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('quaderni_cmm','quaderni_cmm',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
 <div class="pt-4 pb-4" style="background-color: #eee;">
        <div class="row">
            <div class="col-2" align="right" >
                <img style="box-shadow: 5px 5px 5px 0px #000000; width: 5rem; height: 5rem;" src="/SIGMIL/resources/cms/images/Palazzo_Cesi_Facciata_1.jpg"">
            </div>
            <div class="col-10">
                    <h4 style="color: #228B22;"><b>Ruolo dei Magistrati Militari</b></h4>
                   <hr style=" border: 1.2px solid green;margin-right: 30px;">
<i>Ruolo organico dei Magistrati Militari</i>
            </div>
        </div>
    </div>
<hr>
<div id="mainAccordion">
 <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2013</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2014</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2015</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2015_(correzione_errore_materiale)</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2016</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2017</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2018</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2019</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
  
   </div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('ruolo_magistrati','ruolo_magistrati',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
 <div class="pt-4 pb-4" style="background-color: #eee;">
        <div class="row">
            <div class="col-2" align="right" >
                <img style="box-shadow: 5px 5px 5px 0px #000000; width: 5rem; height: 5rem;" src="/SIGMIL/resources/cms/images/Palazzo_Cesi_Facciata_1.jpg"">
            </div>
            <div class="col-10">
                    <h4 style="color: #228B22;"><b>Ruolo dei Magistrati Militari</b></h4>
                   <hr style=" border: 1.2px solid green;margin-right: 30px;">
<i>Ruolo organico dei Magistrati Militari</i>
            </div>
        </div>
    </div>
<hr>
<div id="mainAccordion">
 <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2013</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2014</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2015</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2015_(correzione_errore_materiale)</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2016</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2017</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2018</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#PDF''/>">Ruolo_dei_Magistrati_Militari_al_1_gennaio_2019</a>
               </h5><i> (file PDF) </i>
         </div>
      </div>
   </div>
   </div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('search_result','search_result','jacms',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<h1><@wp.i18n key="SEARCH_RESULTS" /></h1>
<#if (RequestParameters.search?? && RequestParameters.search!='''')>
<@jacms.searcher listName="contentListResult" />
</#if>
<p><@wp.i18n key="SEARCHED_FOR" />: <em><strong><#if (RequestParameters.search??)>${RequestParameters.search}</#if></strong></em></p>
<#if (contentListResult??) && (contentListResult?has_content) && (contentListResult?size > 0)>
<@wp.pager listName="contentListResult" objectName="groupContent" max=10 pagerIdFromFrame=true advanced=true offset=5>
	<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
	<p><em><@wp.i18n key="SEARCH_RESULTS_INTRO" /> <!-- infamous whitespace hack -->
	${groupContent.size}
	<@wp.i18n key="SEARCH_RESULTS_OUTRO" /> [${groupContent.begin + 1} &ndash; ${groupContent.end + 1}]:</em></p>
	<@wp.fragment code="default_pagerBlock" escapeXml=false />
	<#list contentListResult as contentId>
	<#if (contentId_index >= groupContent.begin) && (contentId_index <= groupContent.end)>
		<@jacms.content contentId="${contentId}" modelId="list" />
	</#if>
	</#list>
	<@wp.fragment code="default_pagerBlock" escapeXml=false />
	</@wp.freemarkerTemplateParameter>
</@wp.pager>
<#else>
<p class="alert alert-info"><@wp.i18n key="SEARCH_NOTHING_FOUND" /></p>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('servizi_banche_dati','servizi_banche_dati',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Argomenti e dettagli correlati</i></b></h4>
<hr>
<div id="mainAccordion">
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#BibliotecaCesi''/>">Biblioteca CESI</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#MinisteroDifesa''/>">Ministero della Difesa</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#CSM''/>">C.S.M. - Consiglio Superiore della Magistratura</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#linkscuolaMag''/>">Scuola Superiore della Magistratura</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#webamailpersociv''/>">WebMail di PERSOCIV</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#circolaripersociv''/>">Circolari di PERSOCIV</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#circolaripersociv''/>">Rassegna Stampa Ministero Difesa</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#circolaripersociv''/>">Linea Amica (Il Portale degli Italiani)</a>
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#circolaripersociv''/>">Portale NoiPa</a><br> Portale dei servizi NoiPa
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''siti_banche_dati_giuridiche''/>">Siti e Banche Dati Giuridiche</a><br>Collegamenti a Siti e Banche Dati in materie giuridiche
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''siti_esterni_interesse''/>">Siti Esterni di Interesse
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''contatti''/>">Contatti
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''privacy''/>">Privacy
               </h5>
         </div>
      </div>
   </div>
<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''note_legali''/>">Note Legali
               </h5>
         </div>
      </div>
   </div>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('sicomm','sicomm',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Argomenti e dettagli correlati</i></b></h4>
<hr>
<div id="mainAccordion">
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''#SICOMM''/>">SICOMM - Sistema Informativo del Consiglio della Magistratura Militare</a><br>Accesso diretto alla versione di Esercizio presente presso il Comando Difesa C4
               </h5>
         </div>
      </div>
   </div>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('sigmil','sigmil',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Normativa</i></b></h4>
<hr>
<div id="mainAccordion">
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''sigmil_giustizia_militare''/>">SIGMIL - Sistema Informativo Giustizia Militare</a><br>Accesso diretto alla versione di Esercizio presente presso il Comando Difesa C4
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                  <a title="Organigramma" href="<@wp.url page=''il_progetto_sigmil''/>">Il Progetto SIGMIL</a><br>Presentazione del Progetto e principali caratteristiche tecnologiche del Sistema
               </h5>
         </div>
      </div>
   </div>
	<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                  <a title="Cenni storici" href="<@wp.url page=''documentazione_sigmil''/>">Documentazione informativa sul Programma SIGMIL</a>
               </h5>
         </div>
      </div>
   </div>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('sistemi_informativi','sistemi_informativi',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Sistemi Informativi e Applicazioni</i></b></h4>
<hr>
<div id="mainAccordion">
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''sigmil''/>">SIGMIL</a>
<br>Sistema Informativo per la Giustizia Militare
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                  <a title="Organigramma" href="<@wp.url page=''sicomm''/>">SICOMM</a>
<br>Sistema Informativo del Consiglio della Magistratura Militare
               </h5>
         </div>
      </div>
   </div>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('siti_banche_dati','siti_banche_dati',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Argomenti e dettagli correlati</i></b></h4>
<hr>
<div id="mainAccordion">
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Gazzetta Ufficiale della Repubblica Italiana
                </a></br> Sito web della Gazzetta Ufficiale della Repubblica Italiana
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Normattiva - Il Portale della legge vigente
              </a></br> Il Portale della legge vigente
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Italgiure Web
              </a></br>Banca Dati di Giurisprudenza, Dottrina e Legislazione
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Corte Suprema di Cassazione
              </a></br> Sito della Corte di cassazione
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Banca Dati della Corte Penale Internazionale
              </a></br> Tribunale per crimini internazionali con sede all`Aia (NL)
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Giustizia Amministrativa
              </a></br> Sito web della Giustizia Amministrativa
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">CeRDEF - Documentazione Economica e Finanziaria
              </a></br> Sito di documentazione economica e finanziaria
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Altalex (Quotidiano di informazione giuridica)
              </a></br> Sito web di Altalex (Quotidiano di informazione giuridica)
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Notiziario Giuridico Telematico
              </a></br> Sito web del Notiziario Giuridico Telematico (Raccolta di siti di interesse giuridico)
             </h5>
       </div>
    </div>
 </div>

 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Diritto Penale Contemporaneo
              </a></br>Sito di documentazione giuridica `Diritto Penale Contemporaneo
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Sito Penale.it
              </a></br>Sito web Penale.it (Diritto, procedura e pratica penale)
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Sistema LE LEGGI D`ITALIA (Pubblica Amministrazione)
              </a></br>Sistema LE LEGGI D`ITALIA (De Agostini) per la Pubblica Amministrazione
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">De Jure - Edizioni Giuffr�
              </a></br> Sito `De Jure` delle Edizioni Giuffr�
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Corte Europea dei Diritti dell`Uomo
              </a></br>Sito della Corte Europea dei Diritti dell`Uomo
             </h5>
       </div>
    </div>
 </div>
 <div class="p-2 mb-1">
    <div id="headingZero">
       <div class="row">
             <h5 class="col-10 align-self-center mb-0">
             <i class="fa fa-chevron-right orgicon"></i>
               <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Corte di Giustizia dell`Unione Europea
              </a></br>Sito della Corte di Giustizia dell`Unione Europea
             </h5>
       </div>
    </div>
 </div>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('siti_esterni','siti_esterni',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Collegamenti diretti:</i></b></h4>
<hr>
<div id="mainAccordion">
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Ministero della Difesa</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Esercito Italiano</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Marina Militare</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Aeronautica Militare</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Arma dei Carabinieri</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Guardia di Finanza</a>
               </h5>
         </div>
      </div>
   </div>
</div>
<br>
<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Collegamenti per categoria o raggruppamento:</i></b></h4>
<hr>
<div id="mainAccordion">
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Comandi ed Enti della Difesa</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Pari Opportunit�</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Siti Istituzionali Italiani</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''assetto_giustizia_militare''/>">Siti Internazionali</a>
               </h5>
         </div>
      </div>
   </div>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('ufficiali_giudici_militari','ufficiali_giudici_militari',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<h4 style="color: #228B22;"><b><i>Argomenti e dettagli correlati</i></b></h4>
<hr>
<div id="mainAccordion">
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Assetto della Giustizia Militare" href="<@wp.url page=''elenchi_giudici_militari''/>">Elenchi degli Ufficiali designabili Giudici Militari</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                  <a title="Organigramma" href="<@wp.url page=''cenni_generali''/>">Cenni generali</a>
               </h5>
         </div>
      </div>
   </div>
	<div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                  <a title="Cenni storici" href="<@wp.url page=''normativa''/>">Normativa</a>
               </h5>
         </div>
      </div>
   </div>
   <div class="p-2 mb-1">
      <div id="headingZero">
         <div class="row">
               <h5 class="col-10 align-self-center mb-0">
               <i class="fa fa-chevron-right orgicon"></i>
                 <a title="Ufficiali Giudici militari" href="<@wp.url page=''esonero_funzioni''/>">Esonero dalle funzioni</a><br>
               </h5>
         </div>
      </div>
   </div>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_editCurrentUser_password','userprofile_editCurrentUser_password',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<h1><@wp.i18n key="userprofile_EDITPASSWORD" /></h1>

<#if (Session.currentUser != "guest") >

	<form action="<@wp.action path="/ExtStr2/do/Front/CurrentUser/changePassword.action" />" method="post" class="form-horizontal">

	<@s.if test="hasFieldErrors()">
		<div class="alert alert-block">
			<p><strong><@wp.i18n key="userprofile_MESSAGE_TITLE_FIELDERRORS" /></strong></p>
			<ul class="unstyled">
				<@s.iterator value="fieldErrors">
					<@s.iterator value="value">
						<li><@s.property escapeHtml=false /></li>
					</@s.iterator>
				</@s.iterator>
			</ul>
		</div>
	</@s.if>

	<p class="noscreen">
		<wpsf:hidden name="username" />
	</p>

	<div class="control-group">
		<label for="userprofile-old-password" class="control-label"><@wp.i18n key="userprofile_OLDPASSWORD" /></label>
		<div class="controls">
			<@wpsf.password
				useTabindexAutoIncrement=true
				name="oldPassword"
				id="userprofile-old-password" />
		</div>
	</div>

	<div class="control-group">
		<label for="userprofile-new-password" class="control-label"><@wp.i18n key="userprofile_NEWPASS" /></label>
		<div class="controls">
			<@wpsf.password
				useTabindexAutoIncrement=true
				name="password"
				id="userprofile-new-password" />
		</div>
	</div>

	<div class="control-group">
		<label for="userprofile-new-password-confirm" class="control-label"><@wp.i18n key="userprofile_CONFIRM_NEWPASS" /></label>
		<div class="controls">
			<@wpsf.password
				useTabindexAutoIncrement=true
				name="passwordConfirm"
				id="userprofile-new-password-confirm" />
		</div>
	</div>

	<p class="form-actions">
		<@wp.i18n key="userprofile_SAVE_PASSWORD" var="userprofile_SAVE_PASSWORD" />
		<@wpsf.submit
			useTabindexAutoIncrement=true
			value="%{#attr.userprofile_SAVE_PASSWORD}"
			cssClass="btn btn-primary" />
	</p>

	</form>

<#else>
	<p>
		<@wp.i18n key="userprofile_PLEASE_LOGIN_TO_EDIT_PASSWORD" />
	</p>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_editCurrentUser_profile','userprofile_editCurrentUser_profile',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<h1><@wp.i18n key="userprofile_EDITPROFILE_TITLE" /></h1>
<#if (Session.currentUser != "guest") >
	<form action="<@wp.action path="/ExtStr2/do/Front/CurrentUser/Profile/save.action" />" method="post" class="form-horizontal">
	<@s.if test="hasFieldErrors()">
		<div class="alert alert-block">
			<p><strong><@wp.i18n key="userprofile_MESSAGE_TITLE_FIELDERRORS" /></strong></p>
			<ul class="unstyled">
				<@s.iterator value="fieldErrors">
					<@s.iterator value="value">
						<li><@s.property escapeHtml=false /></li>
					</@s.iterator>
				</@s.iterator>
			</ul>
		</div>
	</@s.if>
	<@s.set var="lang" value="defaultLang" />
	<@s.iterator value="userProfile.attributeList" var="attribute">
		<@s.if test="%{#attribute.active}">
			<@wpsa.tracerFactory var="attributeTracer" lang="%{#lang.code}" />
				<@s.set var="i18n_attribute_name">userprofile_<@s.property value="userProfile.typeCode" />_<@s.property value="#attribute.name" /></@s.set>
				<@s.set var="attribute_id">userprofile_<@s.property value="#attribute.name" /></@s.set>
				<@wp.fragment code="userprofile_is_IteratorAttribute" escapeXml=false />
		</@s.if>
	</@s.iterator>

	<p class="form-actions">
		<@wp.i18n key="userprofile_SAVE_PROFILE" var="userprofile_SAVE_PROFILE" />
		<@wpsf.submit useTabindexAutoIncrement=true value="%{#attr.userprofile_SAVE_PROFILE}" cssClass="btn btn-primary" />
	</p>

	</form>
<#else>
	<p>
		<@wp.i18n key="userprofile_PLEASE_LOGIN" />
	</p>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_currentWithoutProfile',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<h1><@wp.i18n key="userprofile_EDITPROFILE_TITLE" /></h1>
<p class="label label-info">
	<@wp.i18n key="userprofile_CURRENT_USER_WITHOUT_PROFILE" />
</p>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_entryCurrentProfile',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<#if (Session.currentUser != "guest") >
<form action="<@wp.action path="/ExtStr2/do/Front/CurrentUser/Profile/save.action" />" method="post" class="form-horizontal">
	<@s.if test="hasFieldErrors()">
		<div class="alert alert-block">
			<p><strong><@wp.i18n key="userprofile_MESSAGE_TITLE_FIELDERRORS" /></strong></p>
			<ul class="unstyled">
				<@s.iterator value="fieldErrors">
					<@s.iterator value="value">
						<li><@s.property escapeHtml=false /></li>
					</@s.iterator>
				</@s.iterator>
			</ul>
		</div>
	</@s.if>
	<@s.set var="lang" value="defaultLang" />
	<@s.iterator value="userProfile.attributeList" var="attribute">
		<@s.if test="%{#attribute.active}">
			<@wpsa.tracerFactory var="attributeTracer" lang="%{#lang.code}" />
			<@s.set var="i18n_attribute_name">userprofile_<@s.property value="userProfile.typeCode" />_<@s.property value="#attribute.name" /></@s.set>
			<@s.set var="attribute_id">userprofile_<@s.property value="#attribute.name" /></@s.set>
			<@wp.fragment code="userprofile_is_IteratorAttribute" escapeXml=false />
		</@s.if>
	</@s.iterator>
	<p class="form-actions">
		<@wp.i18n key="userprofile_SAVE_PROFILE" var="userprofile_SAVE_PROFILE" />
		<@wpsf.submit useTabindexAutoIncrement=true value="%{#attr.userprofile_SAVE_PROFILE}" cssClass="btn btn-primary" />
	</p>
</form>
<#else>
	<p><@wp.i18n key="userprofile_PLEASE_LOGIN" /></p>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-AllList-addElementButton',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<@s.set var="add_label"><@wp.i18n key="userprofile_ADDITEM_LIST" /></@s.set>

<@wpsa.actionParam action="addListElement" var="actionName" >
	<@wpsa.actionSubParam name="attributeName" value="%{#attribute.name}" />
	<@wpsa.actionSubParam name="listLangCode" value="%{#lang.code}" />
</@wpsa.actionParam>
<@s.set var="iconImagePath" id="iconImagePath"><@wp.resourceURL/>administration/common/img/icons/list-add.png</@s.set>
<@wpsf.submit
	cssClass="btn"
	useTabindexAutoIncrement=true
	action="%{#actionName}"
	value="%{add_label}"
	title="%{i18n_attribute_name}%{'': ''}%{add_label}" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-BooleanAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<label class="radio inline" for="<@s.property value="%{#attribute_id + ''-true''}" />">
	<@wpsf.radio
		useTabindexAutoIncrement=true
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		id="%{#attribute_id + ''-true''}"
		value="true"
		checked="%{#attribute.value == true}"
		cssClass="radio" />
		<@wp.i18n key="userprofile_YES" />
</label>
&#32;
<label class="radio inline" for="<@s.property value="%{#attribute_id+''-false''}" />">
	<@wpsf.radio
		useTabindexAutoIncrement=true
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		id="%{#attribute_id + ''-false''}"
		value="false"
		checked="%{#attribute.value == false}"
		cssClass="radio" />
		<@wp.i18n key="userprofile_NO" />
</label>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-CheckboxAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<@wpsf.checkbox useTabindexAutoIncrement=true
	name="%{#attributeTracer.getFormFieldName(#attribute)}"
	id="%{attribute_id}" value="%{#attribute.value == true}"/>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-CompositeAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@s.set var="i18n_parent_attribute_name" value="#attribute.name" />
<@s.set var="masterCompositeAttributeTracer" value="#attributeTracer" />
<@s.set var="masterCompositeAttribute" value="#attribute" />
<@s.iterator value="#attribute.attributes" var="attribute">
	<@s.set var="attributeTracer" value="#masterCompositeAttributeTracer.getCompositeTracer(#masterCompositeAttribute)"></@s.set>
	<@s.set var="parentAttribute" value="#masterCompositeAttribute"></@s.set>
	<@s.set var="i18n_attribute_name">userprofile_ATTR<@s.property value="%{i18n_parent_attribute_name}" /><@s.property value="#attribute.name" /></@s.set>
	<@s.set var="attribute_id">userprofile_<@s.property value="%{i18n_parent_attribute_name}" /><@s.property value="#attribute.name" />_<@s.property value="#elementIndex" /></@s.set>
	<@wp.fragment code="userprofile_is_IteratorAttribute" escapeXml=false />
</@s.iterator>
<@s.set var="attributeTracer" value="#masterCompositeAttributeTracer" />
<@s.set var="attribute" value="#masterCompositeAttribute" />
<@s.set var="parentAttribute" value=""></@s.set>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-DateAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<#assign currentLangVar ><@wp.info key="currentLang" /></#assign>

<@s.if test="#attribute.failedDateString == null">
<@s.set var="dateAttributeValue" value="#attribute.getFormattedDate(''dd/MM/yyyy'')" />
</@s.if>
<@s.else>
<@s.set var="dateAttributeValue" value="#attribute.failedDateString" />
</@s.else>
<@wpsf.textfield
useTabindexAutoIncrement=true id="%{attribute_id}"
name="%{#attributeTracer.getFormFieldName(#attribute)}"
value="%{#dateAttributeValue}" maxlength="10" cssClass="text userprofile-date" />
&#32;
<#assign js_for_datepicker="jQuery(function($){
	$.datepicker.regional[''it''] = {
		closeText: ''Chiudi'',
		prevText: ''&#x3c;Prec'',
		nextText: ''Succ&#x3e;'',
		currentText: ''Oggi'',
		monthNames: [''Gennaio'',''Febbraio'',''Marzo'',''Aprile'',''Maggio'',''Giugno'',
			''Luglio'',''Agosto'',''Settembre'',''Ottobre'',''Novembre'',''Dicembre''],
		monthNamesShort: [''Gen'',''Feb'',''Mar'',''Apr'',''Mag'',''Giu'',
			''Lug'',''Ago'',''Set'',''Ott'',''Nov'',''Dic''],
		dayNames: [''Domenica'',''Luned&#236'',''Marted&#236'',''Mercoled&#236'',''Gioved&#236'',''Venerd&#236'',''Sabato''],
		dayNamesShort: [''Dom'',''Lun'',''Mar'',''Mer'',''Gio'',''Ven'',''Sab''],
		dayNamesMin: [''Do'',''Lu'',''Ma'',''Me'',''Gi'',''Ve'',''Sa''],
		weekHeader: ''Sm'',
		dateFormat: ''dd/mm/yy'',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''''};
});

jQuery(function($){
	if (Modernizr.touch && Modernizr.inputtypes.date) {
		$.each(	$(''input.userprofile-date''), function(index, item) {
			item.type = ''date'';
		});
	} else {
		$.datepicker.setDefaults( $.datepicker.regional[''${currentLangVar}''] );
		$(''input.userprofile-date'').datepicker({
      			changeMonth: true,
      			changeYear: true,
      			dateFormat: ''dd/mm/yyyy''
    		});
	}
});" >

<@wp.headInfo type="JS" info="entando-misc-html5-essentials/modernizr-2.5.3-full.js" />
<@wp.headInfo type="JS_EXT" info="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" />
<@wp.headInfo type="CSS_EXT" info="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.min.css" />
<@wp.headInfo type="JS_RAW" info="${js_for_datepicker}" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-EnumeratorAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@wpsf.select useTabindexAutoIncrement=true
	name="%{#attributeTracer.getFormFieldName(#attribute)}"
	id="%{attribute_id}"
	headerKey="" headerValue=""
	list="#attribute.items" value="%{#attribute.getText()}" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-EnumeratorMapAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@wpsf.select useTabindexAutoIncrement=true
	name="%{#attributeTracer.getFormFieldName(#attribute)}"
	id="%{attribute_id}"
	headerKey="" headerValue=""
	list="#attribute.mapItems" value="%{#attribute.getText()}" listKey="key" listValue="value" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-HypertextAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<@wpsf.textarea
	useTabindexAutoIncrement=true
	cols="50"
	rows="3"
	id="%{#attribute_id}"
	name="%{#attributeTracer.getFormFieldName(#attribute)}"
	value="%{#attribute.textMap[#lang.code]}"  />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-LongtextAttribute',NULL,NULL,NULL,'<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@wpsf.textarea useTabindexAutoIncrement=true cols="30" rows="5" id="%{attribute_id}" name="%{#attributeTracer.getFormFieldName(#attribute)}" value="%{#attribute.getTextForLang(#lang.code)}" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-MonolistAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@s.if test="#attribute.attributes.size() != 0">
	<ul class="unstyled">
</@s.if>

<@s.set var="masterListAttributeTracer" value="#attributeTracer" />
<@s.set var="masterListAttribute" value="#attribute" />

<@s.iterator value="#attribute.attributes" var="attribute" status="elementStatus">
	<@s.set var="attributeTracer" value="#masterListAttributeTracer.getMonoListElementTracer(#elementStatus.index)"></@s.set>
	<@s.set var="elementIndex" value="#elementStatus.index" />
	<@s.set var="i18n_attribute_name">userprofile_ATTR<@s.property value="#attribute.name" /></@s.set>
	<@s.set var="attribute_id">userprofile_<@s.property value="#attribute.name" />_<@s.property value="#elementStatus.count" /></@s.set>

	<li class="control-group  <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@s.if test="#attribute.type == ''Composite''">
				<@s.property value="#elementStatus.count" /><span class="noscreen">&#32;<@s.text name="label.compositeAttribute.element" /></span>
				&#32;
				<@s.if test="#lang.default">
					<@wp.fragment code="userprofile_is_front_AllList_operationModule" escapeXml=false />
				</@s.if>
			</@s.if>
			<@s.else>
				<@s.property value="#elementStatus.count" />
				&#32;
				<@wp.fragment code="userprofile_is_front_AllList_operationModule" escapeXml=false />
			</@s.else>
		</label>
		<div class="controls">
			<@s.if test="#attribute.type == ''Boolean''">
				<@wp.fragment code="userprofile_is_front-BooleanAttribute" escapeXml=false />
			</@s.if>
			<@s.elseif test="#attribute.type == ''CheckBox''">
				<@wp.fragment code="userprofile_is_front-CheckboxAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Composite''">
				<@wp.fragment code="userprofile_is_front-CompositeAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Date''">
				<@wp.fragment code="userprofile_is_front-DateAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Enumerator''">
				<@wp.fragment code="userprofile_is_front-EnumeratorAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''EnumeratorMap''">
				<@wp.fragment code="userprofile_is_front-EnumeratorMapAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Hypertext''">
				<@wp.fragment code="userprofile_is_front-HypertextAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Longtext''">
				<@wp.fragment code="userprofile_is_front-LongtextAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Monotext''">
				<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Number''">
				<@wp.fragment code="userprofile_is_front-NumberAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''ThreeState''">
				<@wp.fragment code="userprofile_is_front-ThreeStateAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Text''">
				<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			</@s.elseif>
			<@s.else>
				<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			</@s.else>
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</li>
</@s.iterator>

<@s.set var="attributeTracer" value="#masterListAttributeTracer" />
<@s.set var="attribute" value="#masterListAttribute" />
<@s.set var="elementIndex" value="" />
<@s.if test="#attribute.attributes.size() != 0">
</ul>
</@s.if>
<@s.if test="#lang.default">
	<div class="control-group">
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-AllList-addElementButton" escapeXml=false />
		</div>
	</div>
</@s.if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-MonotextAttribute',NULL,NULL,NULL,'<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@wpsf.textfield useTabindexAutoIncrement=true id="%{attribute_id}"
	name="%{#attributeTracer.getFormFieldName(#attribute)}" value="%{#attribute.getTextForLang(#lang.code)}"
	maxlength="254" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-NumberAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<@s.if test="#attribute.failedNumberString == null">
	<@s.set var="numberAttributeValue" value="#attribute.value"></@s.set>
</@s.if>
<@s.else>
	<@s.set var="numberAttributeValue" value="#attribute.failedNumberString"></@s.set>
</@s.else>
<@wpsf.textfield
		useTabindexAutoIncrement=true
		id="%{#attribute_id}"
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		value="%{#numberAttributeValue}"
		maxlength="254" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-ThreeStateAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<label class="radio inline" for="<@s.property value="%{#attribute_id + ''-none''}" />">
	<@wpsf.radio
		useTabindexAutoIncrement=true
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		id="%{#attribute_id + ''-none''}"
		value=""
		checked="%{#attribute.booleanValue == null}"
		cssClass="radio" />
		<@wp.i18n key="userprofile_BOTH_YES_AND_NO" />
</label>
&#32;
<label class="radio inline" for="<@s.property value="%{#attribute_id + ''-true''}" />">
	<@wpsf.radio
		useTabindexAutoIncrement=true
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		id="%{#attribute_id + ''-true''}"
		value="true"
		checked="%{#attribute.booleanValue != null && #attribute.booleanValue == true}"
		cssClass="radio" />
		<@wp.i18n key="userprofile_YES" />
</label>
&#32;
<label class="radio inline" for="<@s.property value="%{#attribute_id + ''-false''}" />">
	<@wpsf.radio
		useTabindexAutoIncrement=true
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		id="%{#attribute_id + ''-false''}"
		value="false"
		checked="%{#attribute.booleanValue != null && #attribute.booleanValue == false}"
		cssClass="radio" />
		<@wp.i18n key="userprofile_NO" />
</label>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front_AllList_operationModule',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<@s.if test="null == #operationButtonDisabled">
	<@s.set var="operationButtonDisabled" value="false" />
</@s.if>
<div class="btn-toolbar">
	<div class="btn-group btn-group-sm">
		<@wpsa.actionParam action="moveListElement" var="actionName" >
			<@wpsa.actionSubParam name="attributeName" value="%{#attribute.name}" />
			<@wpsa.actionSubParam name="listLangCode" value="%{#lang.code}" />
			<@wpsa.actionSubParam name="elementIndex" value="%{#elementIndex}" />
			<@wpsa.actionSubParam name="movement" value="UP" />
		</@wpsa.actionParam>
		<@wpsf.submit disabled="%{#operationButtonDisabled}" action="%{#actionName}" type="button" cssClass="btn btn-default" title="%{getText(''label.moveInPositionNumber'')}: %{#elementIndex}">
		<span class="icon fa fa-sort-desc"></span>
		<span class="sr-only"><@s.text name="label.moveInPositionNumber" />: <@s.property value="%{#elementIndex}" /></span>
		</@wpsf.submit>

		<@wpsa.actionParam action="moveListElement" var="actionName" >
			<@wpsa.actionSubParam name="attributeName" value="%{#attribute.name}" />
			<@wpsa.actionSubParam name="listLangCode" value="%{#lang.code}" />
			<@wpsa.actionSubParam name="elementIndex" value="%{#elementIndex}" />
			<@wpsa.actionSubParam name="movement" value="DOWN" />
		</@wpsa.actionParam>
		<@wpsf.submit disabled="%{#operationButtonDisabled}" action="%{#actionName}" type="button" cssClass="btn btn-default" title="%{getText(''label.moveInPositionNumber'')}: %{#elementIndex+2}">
		<span class="icon fa fa-sort-asc"></span>
		<span class="sr-only"><@s.text name="label.moveInPositionNumber" />: <@s.property value="%{#elementIndex}" /></span>
		</@wpsf.submit>
	</div>
	<div class="btn-group btn-group-sm">
		<@wpsa.actionParam action="removeListElement" var="actionName" >
			<@wpsa.actionSubParam name="attributeName" value="%{#attribute.name}" />
			<@wpsa.actionSubParam name="listLangCode" value="%{#lang.code}" />
			<@wpsa.actionSubParam name="elementIndex" value="%{#elementIndex}" />
		</@wpsa.actionParam>
		<@wpsf.submit disabled="%{#operationButtonDisabled}" action="%{#actionName}" type="button" cssClass="btn btn-default btn-warning" title="%{getText(''label.remove'')}: %{#elementIndex}">
		<span class="icon fa fa-times-circle-o"></span>
		<span class="sr-only"><@s.text name="label.remove" />: <@s.property value="%{#elementIndex}" /></span>
		</@wpsf.submit>
	</div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front_AttributeInfo',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@s.if test="#attribute.required">
	<abbr class="icon icon-asterisk" title="<@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MANDATORY_FULL" />"><span class="noscreen"><@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MANDATORY_SHORT" /></span></abbr>
</@s.if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front_attributeInfo-help-block',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@s.set var="validationRules" value="#attribute.validationRules.ognlValidationRule" />
<@s.set var="hasValidationRulesVar" value="%{#validationRules != null && #validationRules.expression != null}" />

<@s.if test="%{#hasValidationRulesVar || #attribute.type == ''Date'' || (#attribute.textAttribute && (#attribute.minLength != -1 || #attribute.maxLength != -1))}">
		<span class="help-block">
		<@s.if test="#attribute.type == ''Date''">dd/MM/yyyy&#32;</@s.if>
		<@s.if test="%{#validationRules.helpMessageKey != null}">
			<@s.set var="label" scope="page" value="#validationRules.helpMessageKey" /><@wp.i18n key="${label}" />
		</@s.if>
		<@s.elseif test="%{#validationRules.helpMessage != null}">
			<@s.property value="#validationRules.helpMessage" />
		</@s.elseif>
		<@s.if test="#attribute.minLength != -1">
			&#32;
			<abbr title="<@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MINLENGTH_FULL" />&#32;<@s.property value="#attribute.minLength" />">
				<@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MINLENGTH_SHORT" />:&#32;<@s.property value="#attribute.minLength" />
			</abbr>
		</@s.if>
		<@s.if test="#attribute.maxLength != -1">
			&#32;
			<abbr title="<@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MAXLENGTH_FULL" />&#32;<@s.property value="#attribute.maxLength" />">
				<@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MAXLENGTH_SHORT" />:&#32;<@s.property value="#attribute.maxLength" />
			</abbr>
		</@s.if>
	</span>
</@s.if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_IteratorAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<#assign i18n_attribute_name ><@s.property value="#i18n_attribute_name" /></#assign>
<@s.if test="#attribute.type == ''Boolean''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-BooleanAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.if>
<@s.elseif test="#attribute.type == ''CheckBox''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-CheckboxAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Composite''">
	<div class="well well-small">
		<fieldset class=" <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
			<legend class="margin-medium-top">
				<@wp.i18n key="${i18n_attribute_name}" />
				<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
			</legend>
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
			<@wp.fragment code="userprofile_is_front-CompositeAttribute" escapeXml=false />
		</fieldset>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Date''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-DateAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Enumerator''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-EnumeratorAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''EnumeratorMap''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-EnumeratorMapAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Hypertext''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-HypertextAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''List''">
	<div class="well well-small">
		<fieldset class=" <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
			<legend class="margin-medium-top">
				<@wp.i18n key="${i18n_attribute_name}" />
					<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
			</legend>
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
			<@wp.fragment code="userprofile_is_front-MonolistAttribute" escapeXml=false />
		</fieldset>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Longtext''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-LongtextAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Monolist''">
	<div class="well well-small">
		<fieldset class=" <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
			<legend class="margin-medium-top"><@wp.i18n key="${i18n_attribute_name}" />
				<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
			</legend>
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
			<@wp.fragment code="userprofile_is_front-MonolistAttribute" escapeXml=false />
		</fieldset>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Monotext''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Number''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-NumberAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Text''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''ThreeState''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-ThreeStateAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.else> <#-- for all other types, insert a simple label and a input[type="text"] -->
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.else>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_passwordChanged',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign s=JspTaglibs["/struts-tags"]>

<h1><@wp.i18n key="userprofile_EDITPASSWORD_TITLE" /></h1>
<p class="alert alert-success"><@wp.i18n key="userprofile_PASSWORD_UPDATED" /></p>
<@s.if test="!#session.currentUser.credentialsNonExpired">
	<p class="alert alert-info">
		<a href="<@s.url namespace="/do" action="logout" />" ><@wp.i18n key="userprofile_PLEASE_LOGIN_AGAIN" /></a>
	</p>
</@s.if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_profileChangeConfirmation',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<h1><@wp.i18n key="userprofile_EDITPROFILE_TITLE" /></h1>
<p><@wp.i18n key="userprofile_PROFILE_UPDATED" /></p>',1);
