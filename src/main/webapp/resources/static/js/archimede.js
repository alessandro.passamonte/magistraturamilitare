$(document).ready(function () {

    window.onscroll = function () {
        myFunction()
    };

    $("#root-menu .card-body").each(function (index, elem) {
        if ($(elem).html().trim() == "" || $(elem).children().first().html().trim() == "") {
            $(elem).parent().prev().find(".collapsing-arrow").remove();
        }
    })

    function myFunction() {
        if (window.pageYOffset > 100) {
            $(elementToBeScrolled).addClass("scrolled")
        } else {
            $(elementToBeScrolled).removeClass("scrolled")
        }
    }

    var elementToBeScrolled = [
        "#horizontal-menu",
        "#main-header",
        "#main-content",
        "#search-rubrica"
    ].join(", ")


    $("#search-button-mobile").click(function () {
        showSearchBar()
    })

    function showSearchBar() {
        if ($("#search-rubrica").attr("style")) {
            $("#search-rubrica").removeAttr('style');
        } else {
            $("#search-rubrica").attr('style', 'display:block !important');
        }
    }


    $('#GABfrontPage').carousel({ interval: 5000 });
    $('#SGDfrontPage').carousel({ interval: 5000 });
    $('#SMDfrontPage').carousel({ interval: 5000 });


    
    $("#GABfrontPage .carousel-inner-homepage .carousel-item").first().addClass("active");
    $("#SGDfrontPage .carousel-inner-homepage .carousel-item").first().addClass("active");
    $("#SMDfrontPage .carousel-inner-homepage .carousel-item").first().addClass("active");    


    $("#search-in-sito").click(function () {
        $("#search-in").attr("placeholder", "Cerca nel sito");
        $("#search-type-id").attr("value", $('#search-in-sito').attr("value"));
        $("#button-search-in").html("Cerca nel Sito");
    });
    $("#search-in-rubrica").click(function () {
        $("#search-in").attr("placeholder", "Cerca nella rubrica");
        $("#search-type-id").attr("value", $('#search-in-rubrica').attr("value"));
        $("#button-search-in").html("Cerca in Rubrica");
    });


    $('#serviceCarousel').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1

                }
            }
        ]
    });

    $("#serviceNext").click(function () {
        $("#serviceCarousel").slick('slickNext');
    })

    $("#servicePrev").click(function () {
        $("#serviceCarousel").slick('slickPrev');
    })


    $('#serviceCarousel2').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 6,
        slidesToScroll: 6,
        arrows: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1

                }
            }
        ]
    });
    $("#serviceNext2").click(function () {
        $("#serviceCarousel2").slick('slickNext');
    })

    $("#servicePrev2").click(function () {
        $("#serviceCarousel2").slick('slickPrev');
    })



    $('#newsList').carousel({ interval: 5000 });
    $('#playButtonRes').click(function () {
        $('#newsList').carousel('cycle');
    });
    $('#pauseButtonRes').click(function () {
        $('#newsList').carousel('pause');
    });


    $('#area_multimediale_image_carousel').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: true,
    });
    $('#area_multimediale_image_carousel .slick-list').addClass("h-100");

    var selectedVideo = 0;


    function initVideoPlayer() {
        if (typeof videoHpSources != 'undefined') {
        $("#area_multimediale_video_carousel").attr("poster" ,videoHpSources[selectedVideo].poster );
            $("#area_multimediale_video_carousel").html("<source src=" + videoHpSources[selectedVideo].url + " type='video/mp4'></source>");
            document.getElementById("area_multimediale_video_carousel").load();
        }
    }

    $("#hp-video-carousel-prev").click(function () {
        if ((selectedVideo) > 0) {
            selectedVideo--;
            initVideoPlayer();
        }
        else{
            selectedVideo = videoHpSources.length -1;
            initVideoPlayer();
        }
    })

    $("#hp-video-carousel-next").click(function () {
        if ((selectedVideo) < videoHpSources.length -1) {
            selectedVideo++;
            initVideoPlayer();
        }else if((selectedVideo) == videoHpSources.length -1){
            selectedVideo = 0;
            initVideoPlayer();
        }
    })


    initVideoPlayer();

})

   function delay() {
	$("#delay").attr('style', 'visibility: visible');
   }
   setTimeout(delay, 50);