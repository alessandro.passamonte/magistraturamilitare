var org = org || {};
org.entando = org.entando || {};
org.entando.datatable = org.entando.datatable || {};
org.entando.datatable.RubricaDatatable = function (idTable, context) {

    var italian = {
        "sEmptyTable": "Nessun dato presente nella tabella",
        "sInfo": "Vista da _START_ a _END_ di _TOTAL_ elementi",
        "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
        "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "Visualizza _MENU_ elementi",
        "sLoadingRecords": "",
        "sProcessing": "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i>",
        "sSearch": "Cerca:",
        "sZeroRecords": "La ricerca non ha portato alcun risultato.",
        "oPaginate": {
            "sFirst": "Inizio",
            "sPrevious": "Precedente",
            "sNext": "Successivo",
            "sLast": "Fine"
        },
        "oAria": {
            "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
            "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
        }
    };

    var selectClicked = {};


    function refreshSelect(id, options) {
        //console.log('selectedClicked', selectClicked, 'id ', id)
        if (options) {
            $(id).empty().append('<option value="">Tutti</option>');
            var opt = options;
            if (!Array.isArray(options)) {
                opt = [options];
            }
            opt.forEach(function (val) {
                $(id).append('<option value="' + val + '">' + val + "</option>");
            });
            var selectedValue = selectClicked[id];
            if (selectedValue) {
                $(id).val(selectedValue);
            }

        } else {
            $(id).empty().append('<option value="">Tutti</option>');
        }
    }

    var config = {
        scrollX: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: context + 'datatablecontacts.json',
            type: "POST",
            contentType: "application/json",
            data: function (data) {
                var obj = data;

                obj.columns[3].search.value = $('#filter-grado').val();
                obj.columns[3].search.regex = true;

                obj.columns[4].search.value = $('#filter-forza').val();
                obj.columns[4].search.regex = true;

                obj.columns[10].search.value = $('#filter-ente').val();
                obj.columns[10].search.regex = true;

                obj.columns[11].search.value = $('#filter-organizzazione').val();
                obj.columns[11].search.regex = true;

                obj.columns[12].search.value = $('#filter-seg-uff-div').val();
                obj.columns[12].search.regex = true;

                //console.log('data - obj', obj);
                return JSON.stringify({request: obj});
            },
            dataFilter: function (data) {
                var jsonData = jQuery.parseJSON(data);
                //console.log('dataFilter - jsonData', jsonData.contacts);
                var json = {};

                refreshSelect('#filter-ente', jsonData.contacts.select.ente);
                refreshSelect('#filter-organizzazione', jsonData.contacts.select.ou);
                refreshSelect('#filter-seg-uff-div', jsonData.contacts.select.subOu);
                refreshSelect('#filter-grado', jsonData.contacts.select.grado);
                refreshSelect('#filter-forza', jsonData.contacts.select.forza);


                json.draw = jsonData.contacts.draw;
                json.recordsTotal = jsonData.contacts.recordsTotal;
                json.recordsFiltered = jsonData.contacts.recordsFiltered;
                if (json.recordsFiltered > 0) {
                    if (!Array.isArray(jsonData.contacts.list)) {
                        json.data = [jsonData.contacts.list];
                    } else {
                        json.data = jsonData.contacts.list;
                    }
                } else json.data = [];
                json = JSON.stringify(json);
                return json;
            },
            error: function (xhr, error, thrown) {
                alert('Error reading the contacts list');
            }
        },
        columns: [
            {data: ''},
            {data: 'cognome'},
            {data: 'nome'},
            {data: 'grado'},
            {data: 'forzaArmata'},
            {data: 'dipartimento'}, //{data: 'enteCompleto'}, modifica 17-05-2019
            {data: 'ufficio'},
            {data: 'funzione'}, // Colonna incarico
            {data: 'telefonoInterno'},
            {data: 'telefonoEsterno'},
            {data: 'ente'},
            {data: 'ou'},
            {data: 'subOu'}
        ],
        columnDefs: [
            {
                targets: 0,
                defaultContent: '<i class="rubrica-detail fa fa-search-plus"></i>'
            }
            ,
            {
                "targets": [10, 11, 12],
                "visible": false
            }

        ],
        dom: '<"toolbar-header"lBf>t<"toolbar-bottom" ip>',
        language: italian,
        order: [[1, 'asc']],
        buttons: [
            {extend: 'copy', text: '<i class="fa fa-copy"></i>'},
            {
                extend: 'csv', text: '<i class="fa fa-file-alt"></i>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excel', text: '<i class="fa fa-file-excel"></i>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdf', text: '<i class="fa fa-file-pdf"></i>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print', text: '<i class="fa fa-print"></i>',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]

    };

    function initSelect() {
        $.get(context + 'datatableselectinit.json', function (data) {
            //console.log('initSelect - data', data);
            if (data.select.ente) {
                $('#filter-ente').empty().append('<option value="">Tutti</option>');
                data.select.ente.forEach(function (val) {
                    $('#filter-ente').append('<option value="' + val + '">' + val + "</option>");
                });
            }
            if (data.select.ou) {
                $('#filter-organizzazione').empty().append('<option value="">Tutti</option>');
                data.select.ou.forEach(function (val) {
                    $('#filter-organizzazione').append('<option value="' + val + '">' + val + "</option>");
                });
            }
            if (data.select.subOu) {
                $('#filter-subOu').empty().append('<option value="">Tutti</option>');
                data.select.subOu.forEach(function (val) {
                    $('#filter-seg-uff-div').append('<option value="' + val + '">' + val + "</option>");
                });
            }
            if (data.select.grado) {
                $('#filter-grado').empty().append('<option value="">Tutti</option>');
                data.select.grado.forEach(function (val) {
                    $('#filter-grado').append('<option value="' + val + '">' + val + "</option>");
                });
            }
            if (data.select.forza) {
                $('#filter-forza').empty().append('<option value="">Tutti</option>');
                data.select.forza.forEach(function (val) {
                    $('#filter-forza').append('<option value="' + val + '">' + val + "</option>");
                });
            }

            $('#filter-ente').val("").trigger("change");
            $('#filter-organizzazione').val("").trigger("change");
            $('#filter-seg-uff-div').val("").trigger("change");
            $('#filter-grado').val("").trigger("change");
            $('#filter-forza').val("").trigger("change");

            selectClicked = [];

        });
     $('input[type=search]').val("").keyup().change();

    }

    function changeSelect() {
        $('#filter-ente').change(function () {
            var idx = $.inArray('#filter-ente', selectClicked);
            if (idx === -1) {
                selectClicked['#filter-ente'] = this.value;
            }
            table.draw();
        });
        $('#filter-organizzazione').change(function () {
            var idx = $.inArray('#filter-organizzazione', selectClicked);
            if (idx === -1) {
                selectClicked['#filter-organizzazione'] = this.value;
            }
            table.draw();
        });
        $('#filter-seg-uff-div').change(function () {
            var idx = $.inArray('#filter-seg-uff-div', selectClicked);
            if (idx === -1) {
                selectClicked['#filter-seg-uff-div'] = this.value;
            }
            table.draw();
        });
        $('#filter-grado').change(function () {
            var idx = $.inArray('#filter-grado', selectClicked);
            if (idx === -1) {
                selectClicked['#filter-grado'] = this.value;
            }
            table.draw();
        });
        $('#filter-forza').change(function () {
            var idx = $.inArray('#filter-forza', selectClicked);
            if (idx === -1) {
                selectClicked['#filter-forza'] = this.value;
            }
            table.draw();
        });
    }

    var table = $(idTable).DataTable(config);


    /* istruzione che serve per nascondere la colonna. L'indice parte da 0 nel nostro caso la colonna nascosta è CognomeExt  */
    //table.column(8).visible(false);
    //table.column(9).visible(false);
    //table.column(10).visible(false);

    $("#data-table-rubrica tbody").removeClass("d-none");
    table.buttons().container().appendTo('#example_wrapper .col-md-6:eq(0)');

    changeSelect();

    $('#btn-cancella-filtri').click(function () {
        initSelect();
    });

    /*
     funzione che resituisce il template html che visualizza i dettagli quando di preme la lente d'ingradimento.
     Il valore dell'indice dell'array d equilvale al numero della colonna. Il primo elemento ha indice 0.
     NB: Il valore dell'indice tiene presente di tutte le colonne anche quelle nascoste!
     * */

    function detail(d) {
        // `d` is the original data object for the row
        //console.log('detail', d)
        return '<table class="table table-striped table-bordered" > ' +
            '<tr> <td>Cognome</td> <td>' + d.cognome + '</td> </tr>' +
            '<tr> <td>Nome</td> <td>' + d.nome + '</td> </tr>' +
            '<tr> <td>Grado</td> <td>' + d.grado + '</td> </tr>' +
            '<tr> <td>Funzione</td> <td>' + d.funzione + '</td></tr>' +
            '<tr> <td>Ente</td> <td>' + d.dipartimento + '</td> </tr>' + // modifica 17-05-2019
            '<tr> <td>Telefono Linea Interna</td> <td>' + d.telefonoInterno + '</td> </tr>' +
            '<tr> <td>Telefono Linea Esterna</td> <td>' + d.telefonoEsterno + '</td> </tr>' +
            '</table>';
    }


    $(idTable + ' tbody').on('click', 'tr td i.rubrica-detail', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(detail(row.data())).show();
            tr.addClass('shown');
        }
    });

    var getParameterByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    /* recupero del paramentro search dalla queryString */
    var value = getParameterByName('search');

    if (value) {
        $('input[type=search]').val(value).keyup().change();
        $('input[type=search]').focus();
    }


};
