$(document).ready(function () {
    var hamburger = {
        openButton: document.querySelector('.hamburger-button'),
        closeButton: document.querySelector('.close-menu'),
        nav: document.querySelector('.hamburger'),

        doToggle: function (e) {
            e.preventDefault();
            this.nav.classList.toggle('expanded');
        }
    };

    hamburger.openButton.addEventListener('click', function (e) {
        hamburger.doToggle(e);
        $("body").css("overflow","hidden")
    });

    hamburger.closeButton.addEventListener('click', function (e) {
        hamburger.doToggle(e);
        $("body").css("overflow","")
    });


});


